demo-tools' Code Repository
===========================

This project is to survey several established software verification tools for C
code known in research community. We aim to apply them to verify distributed
programs against their high level protocol specifications.

The repository has three main purposes. First, we store code snippets, scripts
and instructions for tools. Preferably, we can simplify installation for these
verification tools. I mainly focus on existing tools for C code in research
community. Docker containers are used to simplify building and installation
processes. Current plan is to store ``Dockerfile`` for building Docker images
and ``docker-compose.yml`` to spin up containers.

Second, we add examples C code of our interests, that is, distributed programs,
and investigate how we could verify them with existing tools. A major gap is
because existing tools mostly support sequential programs only, but our examples
are inherently concurrent. Another gap is from the difference between abstract
high level protocol spec and low level imperative and socket programming.
Therefore, we also document our literature survey as well as our attempts to
resolve the problem here.

If you reached this repository via BitBucket, you can find a better formatted
documentation on our `Read the Docs <http://demo-tools.rtfd.io>`_ page.

.. toctree::
   :maxdepth: 1
   :caption: Examples

   examples/README
   examples/cmake-proj/README

Tools
-----

+ `SeaHorn <http://seahorn.github.io/>`_
+ `Smack <http://smackers.github.io/>`_
+ `Verifiable C <http://vst.cs.princeton.edu/veric/>`_
+ `CBMC <https://www.cprover.org/cbmc/>`_
+ `CPAchecker <https://cpachecker.sosy-lab.org/>`_
