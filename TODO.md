# To do list

## Examples

+ [ ] Write TLA-style specifications for `TCPEcho` example
+ [ ] Write assertions in `TCPEchoClient4.c`
+ [ ] Convert `TCPEcho` implementation to one state machine with different
      initial states style.

## Tool Installation

+ [ ] Multi-stage build in `smack.Dockerfile` to reduce docker image size
