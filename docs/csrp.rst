CSRP Protocol
=============

The original spec is at http://srp.stanford.edu/design.html.
I rewrote it to be closer to code and hopefully make it look clearer.

The protocol is to use password to authenticate users, so there are a server
(Host) and a client (User). The end goal is that Host and User computes the
session key independently, and when User provides correct password, they'll
calculate the same session key value so that they can trust each other.

The parameters for the protocol are defined as follows.

==  ================================================
g   a generator modulo N (N is a large prime number)
I   User name
s   a randomly generated number called User's salt
p   User password
H   a hash function such as SHA
k   a constant k = H(N, g)
==  ================================================

Initially, Host should have a database or map storing the following entry
``db[I] = (s, v)`` where ``v = g^(H(s, p))`` is called verifier. This can be
considered as initial states.

Then, the authentication process goes as table below. From line 01~06, User provides
user name ``I`` and public key ``A``. Host looks up the verifier value ``v``, but only
shares back via encrypted values ``s`` and ``B``.
From line 07~11, User and Host computes session key ``Ku`` and ``Kh`` separately.
``Ku`` and ``Kh`` must be equal when User provides the correct password.
From line 12~17, it is validating User and Host computed the same session key
so that User can trust Host and vice versa.

Message passing only happens four times at line 02, 05, 13, and 16.
The rests are all local computations

==  =======================  ==============  ===========
#   User                     Host            Explanation
==  =======================  ==============  ===========
01  A := g^a                                 ``a`` is a random number, ``A`` is a temp public User key
02  send (I, A)                              Send User name ``I`` and key ``A`` to Host
03                           (s,v) := db[I]  Lookup up ``I`` from database
04                           B := k*v + g^b  ``b`` is a random number, ``B`` is a temp public Host key
05                           send (s, B)     Send salt ``s`` and key ``B`` to User
06                           u := H(A,B)     Host computes ``u``
06  u := H(A,B)                              User computes ``u``
07  x := H(s,p)                              User types password ``p`` and computes ``x``
08  Su:=(B-k*g^x)^(a + u*x)
09  Ku := H(S)                               User computes session key ``Ku``
10                           Sh :=(A*v^u)^b  ``Sh`` and ``Su`` must be equal, so do ``Kh`` and ``Ku``
11                           Kh := H(S)      Host computes session key ``Kh``
12  M1:=H(A,B,Su)                          
13  send M1                               
14                           verify M1       Host checks if ``M1 == H(A,B,Sh)``
15                           M2:=H(A,M1,Sh)
16                           send M2       
17  verify M2                                User checks if ``M2 == H(A,M1,Su)``
==  =======================  ==============  ===========

Observe that interleaving can happen between 06~09 and 10~11 as Host and User
can compute concurrently. That is, the spec describes only one valid sequence
for the global state machine. It is unclear how to construct a global state
machine represent the spec and allow more behavior.
