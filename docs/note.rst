Note
====

.. toctree::
   :hidden:

   tlsspec
   csrp
   s2n/s2n


-----

Given safety property over only externally visible events
infer property over internal events for monitoring so that
the failure can be detected/prevented earlier.

+ Model (e.g., automata) can be executable but not necessary whitebox
+ Example execution sequences to generalize


TODO for Pythonic IOA model
---------------------------

+ Action effect might be specified as a relation rather than function.

More evaluation subjects
------------------------

+ Synchronizers (Section 16 in Distributed Algorithms)

  + Required property on User automaton

    - Well-formed execution alternating between send and recv actions
      (safety property)
    - Each user automaton eventually sends message for round *r+1* after
      receiving message for any round *r*.

  + Prove global automaton provides synchronization?

    - It is not discussed in the textbook but left as an exercise.

  + Prove simulation relation from Local Synchronizer(s) to Global Synchronizer

+ Failure Detection
+ Clock Synchronization
+ Other examples that require simulation relations


Amazon FreeRTOS
---------------

https://github.com/aws/amazon-freertos

IP/TCP/HTTP stack for IoT devices
encryption SDK

MQTT: publisher-subscriber model
CBOR: Binary Object Representation
OTA: Over the air update
Shadow: 

Generate correctness conditions and instrument assertions to C code?

+ Code location marking the beginning of an event, a watcher?


Code generation from Koord to C code
------------------------------------

This is a fairly interesting direction. Koord level verification would be the
guarantee for the final composed system.
The generate C code can be restrict to the subset of C supported by CBMC.
CBMC can then verify C code against a model which refines the Koord model for
one component.

Further, we can probably manually inspect how the generated code differs from
the ones written in open source projects. E.g., Amazon FreeRTOS.

Code generation from Dafny to lower level code
----------------------------------------------

+ Generate synthesizable Dafny implementation conforming Dafny IOA specification
+ Synthesize Java code from Dafny implementation

    - Talk to Rustan?

Parsing IOA language?
---------------------

The difficult part of parsing is to support complicated type system and to use
the types in predicates and statements. We could implement a parser strictly
follow the IOA manual, but it will be cumbersome to translate LSL types and

Assuming typed messages is enough for a fragment of properties
--------------------------------------------------------------

In "Typing and Compositionality for Security Protocols: A Generalization to the
Geometric Fragment", they discuss that if security properties are state formulas
specified within **Geometric Fragment**, we can design analysis to only find
attacks using welled-typed messages. They made the claim that if one can find an
attack with ill-typed messages, there must exist an attack with only well-typed
messages.

What I have set up
------------------

+ A modelling and proof framework for bounded protocol instantiations via I/O
  automata with parameters in Dafny (powered by SMT+lfp)
+ Parameters enable explicit specification of network topology
+ Larch modelling with state of the art SMT solver
+ Possibly correct by construction C# code from Dafny

Difficulties in modeling Equational Theory plus Sets in Dafny
-------------------------------------------------------------

The problem arises when checking if a term is derivable from a set of knowledge.
The membership query by Dafny does not consider equality introduced by
axioms. Naïve encoding just checks if there exists a term in the set ``Equal``
to the given term.

+ Inefficient compared with rewriting based approaches

+ Dafny does not model exact sets and sequences. In addition, there are some
  approximation in place for exists/forall quantifiers.

    - With soundness guarantee, i.e., if assertions/post-conditions are proven
      hold, they should hold. The downside is spurious counterexample, so we
      are having problems in finding real attack traces.
    - The approximation is done at Dafny to Boogie translation. Changing
      verification engine for Boogie is unlikely to help. 
    - We avoided this by using only bounded size of sequence. We cannot do the
      same trick because attacker should keep all past knowledge.

Alternatives:

+ A different model from term rewriting and equations

+ A better encoding that is logically equivalent or equi-satisfiable

+ Work on Boogie instead of Dafny?

  - We lose the translation from Dafny to C#.

+ Properties and protocols other than security properties?

Arguments that term rewriting based approach is good enough
-----------------------------------------------------------

In "Soundness of Formal Encryption in the Presence of Active Adversaries",
they argue that, if the encryption scheme is IND-CCA secure, a property proven
in symbolic model implies the property almost holds in computational model
(fail with negligible probability).



Encoding Scyther Semantics in Dafny (or Boogie, or SMT solver)
--------------------------------------------------------------

Agent to role instantiation map becomes redundant because we can explicitly use
free variables and their valuations for the mapping. In I/O automaton level, we
treat them as parameters for the automaton. Each automaton should be provided
with all agents participating in this protocol. The role of an agent can be
found by its position in the parameter list.

We should be able to apply the same method for the variables appearing in term
matching for ``recv`` events.

One potential issue for this method is that there can be `n`:sup:`m` different
instances when there are `n` roles and `m` agents considered, and it becomes
worse when verifying composition on possibly infinite self duplication. The good
news is it is common that `n <= 2` for authentication and secrecy protocols,
and it's mentioned in [Cremers09]_ that exploring up to `m <= 3` agents is
sufficient for `n <= 2` roles. The remaining problem is still the possibly
infinite duplication.


.. [Cremers09] "Comparing State Spaces in Automatic Security Protocol Analysis",
               Formal to Practical Security

Questions in I/O automaton modelling
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Currently we separate Attacker from Environment and make ``recv`` an input
action for all automata to model broadcasting. This limits the capability of
Attacker because Attacker can no longer decide to hide a message. Either
Environment drops the message, or all other automata see the message.

Specialized Logic for cryptography
----------------------------------

Nonce : a possibly infinite set of nonce (number used only once) value
Agent : a finite set of agents
Atom = Nonce ∪ Agent

PT : the set of plain text messages seq
CT : the set of encrypted messages (cipher text)

Symmetric Encryption
~~~~~~~~~~~~~~~~~~~~
K : a (possibly infinite) set of keys for symmetric encryption

Enc : K -> PT -> CT
Dec : K -> CT -> PT ∪ {Invalid} // It is unclear whether you can decipher everything in CT

Axioms:
  // Encryption functions must be injective
  ∀ k ∈ K, p, q ∈ PT, Enc(k, p) = Enc(k, q) => p = q
  // Decipher ciphered text with the same key
  ∀ k ∈ K, p ∈ PT, Dec(k, Enc(k, p)) = p
  // Decipher message with a wrong key will return invalid value
  ∀ k1, k2 ∈ K, p ∈ PT, k1 ≠ k2 => Dec(k1, Enc(k2, p)) = Invalid


Asymmetric Encryption
~~~~~~~~~~~~~~~~~~~~~

PK : a finite set of public  keys for agents
SK : a finite set of private keys for agents



Formalization of Security Protocol Standards
--------------------------------------------

Roles in Protocol Specification
   Executable models with initial knowledge (e.g., private keys of an agent).
   It is parameterized with agents representing itself or other roles.
   In IronFleet, it can done by automaton with Mode and EndPoint as parameters.

Agents - :math:`Agents`
    Real endpoints executing roles

Compromised Agents
    Adversary has knowledge from those compromised agents; therefore it can
    pretend to be any of the compromised agents.

System
    Sys ≜ Adversary ‖ (Arbitrary instantiation of roles on :math:`Agents`)

    Note that an agent can run multiple roles at the same time. Even if the
    number of agents is finite. It can still run infinitely many duplicated
    roles.

Restricting System
    Only finite number of replications of each role

Secrecy
    Adversary cannot infer private keys of honest (not compromised) agents.

Authentication
    .. todo:: Find a suitable definition for authentication

Question: What is the State and Trace in our formalism?
    It seems to me that a state defined in either π-calculus or rewriting is
    a set of inferable knowledge of all possible adversaries. However, assume we
    have an adversary that can obtain secrets, even we directly execute these
    automata, at best it is one (symbolic) execution.

Misc
~~~~

Concluded in [ESOP2003]_

+ Only a single dishonest (compromised) agent needs to be considered for secrecy
  and authentication properties.
+ For the analysis of secrecy, only a single honest agent is sufficient.
+ For the analysis of authentication for protocols with two roles, we only need
  two honest agents.

.. [ESOP2003] Security properties: two agents are sufficient, ESOP 2003


Scyther semantics
-----------------

nonce (Fresh Value)
~~~~~~~~~~~~~~~~~~~

Nonce or fresh value should be distinct each time being generated.

State
~~~~~

A state is defined as a pair of Attacker knowledge (set of terms) and set of
Runs.

Bag of Attacker's knowledge can be considered as a global variable of the whole
system. The drawback is that there is no standalone Attacker automaton, so it is
not so modular as we expected.

Each run is a unique instantiation of a role in the protocol spec. During the
rewriting transition, first event in the run is consumed. The instantiation
is explicitly modeled with ``create`` event.
See Section 3.4 and 3.5 for example program executions.

The instantiation easily creates redundant runs by design. For example, it can
instantiate an Initiator role without any Responder role. This may lead to a
coarse over-approximation as one can assign Roles with Agents arbitrarily.

Actions
~~~~~~~

An action is a pair of 1) an instantiation (partial map) from variables to some
constants or fresh values and 2) an event with pattern matching on data being
sent/received. That is, we have to encode pattern matching as enabling
conditions.

Term Matching
~~~~~~~~~~~~~

Finding a instantiation is done by ``Match(inst, pattern, message, inst')``.
Essentially, this is a special case of unification called `term matching` or
`term subsumption`. There is a built-in feature named ``subsumes_term/2`` in
Prolog. It checks if there is a substitution so that the first term
(``pattern``) can be made equivalent to the second term (``message``). However,
in our work, we might have to enumerate all possible substitutions. We may be
able to argue that there is always only one substitution because there shouldn't
be any variable in message term.


One-Sided Secrecy protocol
~~~~~~~~~~~~~~~~~~~~~~~~~~

See Sect. 4.1 in "Operational Semantics and Verification of Security Protocols"
for more detail of this example. Below is my understanding of the semantics of
the model of their example.

.. math::

    OSS(i) =& ( \{i, r, ni, pk(r)\}, \\
            & [ send_1 (i, r, \{\lvert i, ni \rvert\}_{pk(r)}), \\
            &   claim_2(i, secret, ni)])

    OSS(r) =& ( \{i, r, sk(r), pk(r)\}, \\
            & [ recv_1 (i, r, \{\lvert i, W \rvert\}_{pk(r)}), \\
            &   claim_3(r, secret, W)]) \\

A normal **execution** is as follows. (We skipped Role to Agent assignment,
run ID, and states of other processes here.)

.. math::

       & \langle \{i, r, pk(r)\} \rangle \\
  \xrightarrow{(\emptyset, send_1(i, r, \{\lvert i, ni \rvert\}_{pk(r)}))}
       & \langle \{i, r, pk(r), \{\lvert i, ni \rvert\}_{pk(r)}\} \rangle \\
  \xrightarrow{(\{W \mapsto ni\}, recv_1(i, r, \{\lvert i, W \rvert\}_{pk(r)}))}
       & \langle \{i, r, pk(r), \{\lvert i, ni \rvert\}_{pk(r)}\} \rangle \\
  \xrightarrow{(\{W \mapsto ni\}, claim_2(i, secret, ni))}
       & \langle \{i, r, pk(r), \{\lvert i, ni \rvert\}_{pk(r)}\} \rangle \\
  \xrightarrow{(\{W \mapsto ni\}, claim_3(r, secret, W))}
       & \langle \{i, r, pk(r), \{\lvert i, ni \rvert\}_{pk(r)}\} \rangle \\

The attacker cannot decipher :math:`ni` because it does not have the knowledge of
:math:`sk(r)`.

An attack **execution** is as follows. (We skipped Role to Agent assignment,
run ID, and states of other processes here.)

.. math::

       & \langle \{i, r, ne, pk(r)\} \rangle \\
  \xrightarrow{(\{W \mapsto ne\}, recv_1(i, r, \{\lvert i, W \rvert\}_{pk(r)}))}
       & \langle \{i, r, ne, pk(r)\} \rangle \\
  \xrightarrow{(\{W \mapsto ne\}, claim_3(r, secret, W))}
       & \langle \{i, r, ne, pk(r)\} \rangle \\

The secrecy claim, :math:`clame_3`, does not hold in this execution because the
received message, :math:`ne`, is sent by the attacker. Note that the attacker
also need the knowledge of :math:`i` and :math:`pk(r)` to construct the term
:math:`\{\lvert i, ne \rvert\}_{pk(r)}` so that it matches the messages for
:math:`recv_1`.

Some observations for this model

+ The attacker sending any derivable message is implicitly stated in the
  semantics.

  - We can explicitly use an environment automaton and all other automata must
    send messages to it.
  - Symbolically representing all derivable messages is doable, but how do we
    match the pattern in `recv` events and continue the execution.

Needham-Schroeder Protocol
~~~~~~~~~~~~~~~~~~~~~~~~~~

In order to generate attack to this protocol, we'll have to model authentication
property, dishonest agents, and role to agent instantiation.

Evaluation Subjects
-------------------

+ `Amazon s2n <https://github.com/awslabs/s2n>`_ for TLS/SSL protocols

  - My own notes for :doc:`s2n/s2n`

+ `csrp <https://github.com/cocagne/csrp>`_ for Secure Remote Password (SRP) protocol

  - 829 lines of code including tests
  - Depend on existing encryption libraries in OpenSSL
  - Does not really use socket APIs to send/receive messages
  - My own notes for :doc:`csrp`

+ `libjwt <https://github.com/benmcollins/libjwt>`_ for JSON Web Tokens

  - <3K lines of code including tests
  - Depend on existing encryption libraries and file formats in OpenSSL
  - Depend on jansson, a library manipulating JSON format,
    less than 8K lines of code

Verification Framework Support
------------------------------

+ C code

  - `SeaHorn <http://seahorn.github.io/>`_
  - `Verifiable C <http://vst.cs.princeton.edu/veric/>`_
  - `Smack <http://smackers.github.io/>`_

    - Combine with `Dafny <https://github.com/Microsoft/dafny>`_ to approach
      proposed in project `IronClad <https://github.com/Microsoft/Ironclad>`_
    - Smack seems to be already used in s2n project

  - `kcc <https://github.com/kframework/c-semantics>`_

Useful Options for Tools
------------------------

Dafny
~~~~~

/compile n          Put ``0`` for ``n`` to only verify Dafny program and stop
                    compiling to dll
/print file         To output generated Boogie files
/proc <p>           Only check **Boogie** procedures matched by pattern ``<p>``.
                    Use * as wildcard.
/proverLog file     To output generated SMT formulas in SMT-LIB2 format
/mv file            Specify file where to save the model in BVD format


Uncategorized Notes
-------------------

+ How does shared memory come in here?

  - Every agent is identical

+ Discussion with Ritwika

  - Concurrency in our setting can be simplified first. We require strong
    guarantee that every transition by an event should preserve safeness.
    Therefore, interleaving between agents does not affect safety

    + Relax the guarantee later to one delta cycle instead of each event

  - Post_e is assumed given to generate Verification Conditions from krd code,
    so the synthesis/inference problem is modeled as generating FOL formulas
    satisfying constraints that describes inductiveness.

+ Discussion with Madhu

  - Use Sketch to try out some simple examples

    + http://people.csail.mit.edu/asolar/sketch2012/

  - Problem could come from continuous dynamics

    + Other techniques that specialized for continuous dynamics
