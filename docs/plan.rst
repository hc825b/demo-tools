Work Plan
=========

TODO
----

Writing
~~~~~~~

+ Encoding security protocol to I/O automata

  - Definition of agents
  - Compatible parameter valuation and topology of network
  - Security properties over I/O automata

      + Secrecy
      + Forward Secrecy
      + Authentication
      + Unique Channel Identifier

+ I/O automata to Dafny (or more general in SMT theories)

  - Probably only need to say that the translation done by Dafny is similar
    to Larch at least for predicates and immutable data types

Coding
~~~~~~

+ Encode authentication for Smart Meters

  - Add equality theories for XOR, Hash, and Diffie-Hellman


More
----

+ Formalize NS example as I/O automata in Dafny

  - Encode **authentication property**
  - (Optional) Encode composition as an operation over a sequence of automata

+ Survey and decide how to encode encryption, decryption, and structured message
  symbolically

  - Encode whole term rewriting scheme in FOL+lfp
  - Encode uninterpreted encryption and decryption functions with axioms
  - Integrate an external unification engine, e.g., Prolog, to generate answers

+ Check if our model of the attacker is more powerful or not

  - Passive and Active

+ Refinement proof from protocol to specification

+ Connect Dafny high level spec to implementation

  - Refinement proof from implementation to protocol for Echo TCP
  - Can we insert precondition and post-conditions to the code and prove
    using existing tools

+ Check Dr. Shuo Chen's papers. He is in MSR RiSE group

+ Investigate how IronFleet model channel

  - In IronFleet, channel automaton seems to be a queue receiving messages in each agent.

+ Prove Echo system with channel

  - Add Model of the channel (Message passing with error or drop)
  - Separate send and receive actions

+ C/Java program (or any implementation) of simple distributed algorithms
  as benchmarks

  - Evaluate efforts to do preprocessing to an abstraction reasonably
  - Well-known invariant so that we only need to check if our invariant implies
    them and still inductive
  - Demonstrate bugs due to incorrect transition system
    instead of encryption code
    
+ Survey modeling pi-calculus in Promela
    
Submission Deadline
-------------------

==========  ===============  =================  ==
Conference  Due              Venue              Misc
==========  ===============  =================  ==
CONCUR      Apr. 15, 2019    Amsterdam
ATVA        Apr. 26, 2019    Taipei             Academia Sinica
RTSS        May 30, 2019     Hong Kong
==========  ===============  =================  ==


Broad Directions
----------------

+ Verify protocol design against specifications

  - Prove properties such as secrecy for TLS protocols
  - Construct adversary and traces as witness when proof can't be derived

+ Prove low level implementation refines protocol design

  - Connect semantics of low level code to semantics of protocol design, i.e.,
    Label Transition Systems to imperative programs
  - Generate assertions in code that establish the refinement proof so that
    one can use existing verification tools for code

+ Propose different formalism for protocol design. Use I/O automata instead of
  π-calculus, rewriting system, or functional language with dependent types

  - Provide benefits not available in existing formulations

+ Generate Protocol layer design, i.e., spec for code implementation

  Option 1.
    Derive from global spec. Each node is the same automaton but with different
    initial states, and therefore takes different transitions.
  Option 2.
    Extract certain level of abstraction from code as protocol design and
    verify against global spec. Possibly work on multiple implementations of
    the same protocol.

+ Continuous verification via composition based approach

  - Incremental Verification <-> Regression Testing

    + Is it straight-forward to cache and reuse existing proofs if decomposed
      spec is provided?
    + Impact of changes in (mid layer) spec versus changes in code

      - New code against old spec
      - New spec against old code

  - Parallel or distributed verification algorithms to enable cloud platform

Subjects
~~~~~~~~

+ MQTT

  - Publisher-subscriber model built on top of server-client communication
  - E.g., MQTT on top of TCP

+ TLS
+ Authorization Protocols

  - OAuth
  - JSON Web Token

+ Routing Protocols
+ Other consensus protocols not done by IronFleet
+ Other distributed protocols?

  - Protocols in Bluetooth Stack

