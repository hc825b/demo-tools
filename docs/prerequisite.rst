Prerequisites
=============

Basic knowledge with Docker containerization is required to use the container
images and execute commands within the containers. Every ``Dockerfile`` is
tested with `Docker Community Edition`_ on Ubuntu.

.. todo::

    Basic commands to spin up containers


.. _Docker Community Edition: https://www.docker.com/community-edition

