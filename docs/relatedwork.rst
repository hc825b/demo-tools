Related Work
============

My own questions

+ Is it necessary to propose a different formulation based on Labeled Transition
  System (LTS) knowing that there are three different formulations already?

+ ProVerif (π-calculus) can reason with process compositions, passing channels
  as messages, bi-simulation between processes, and even infinite sessions.
  Still below are some disadvantages I can think of.

  - Modeling noisy or lossy channels in ProVerif might not be modular?
  - Limited ability of **Active Adversary**?
  - LTS is closer to imperative programming and therefore we can generate
    specifications or contracts for low level implementation
  - Reason with trace refinement instead of bi-simulation
  - Unclear how to reconstruct executable attacker

Project Everest
---------------

https://project-everest.github.io/

+ Implementation should be F\* program, functional program with dependent type
+ Specification and verification done on F\*
+ Eventually synthesized to C (Clight in CompCert) code using
  `KreMLin <https://github.com/FStarLang/kremlin/>`_

  - F* implementation of TLS, https://github.com/mitls/mitls-fstar
  - Synthesized C code in HACL* https://github.com/mitls/hacl-star/tree/master/snapshots
  - Synthesized C code are only for cryptography algorithms

Software Analysis Workbench
---------------------------

https://saw.galois.com/

+ SAW focused more on the correctness of C code for cryptography algorithms
+ Implementation can be C or Java
+ Specification language `Cryptol <https://cryptol.net/>`_
+ Translate LLVM IR, JVM bytecode, and Cryptol code to modeling language
  `SAWCore <https://github.com/GaloisInc/saw-core>`_
+ Verification done on SAWCore
+ They used it to verify a component HMAC in Amazon s2n
+ They also have verified some C/Java examples for demo
  https://github.com/GaloisInc/saw-script/tree/master/examples

Tamarin
-------

https://tamarin-prover.github.io/

+ Multi-set Rewriting System

ProVerif
--------

http://prosecco.gforge.inria.fr/personal/bblanche/proverif/

+ π-calculus extended with cryptography and data manipulation primitives
+ ProVerif model for TLS 1.2 and 1.3 with reference Flow implementation
  (static type checked JavaScript) at
  `RefTLS <https://github.com/Inria-Prosecco/reftls>`_

