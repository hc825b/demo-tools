s2n
===

s2n TLS Transition System
-------------------------

Action Table
~~~~~~~~~~~~

This section is excerpted from
https://github.com/awslabs/s2n/blob/master/docs/DEVELOPMENT-GUIDE.md
I only updated the code snippets with current revision ``dd716c2``. Read this
code and compare with Figure 1 at :rfc:`5246#page-36`.

Every connection is associated with an ``s2n_connection`` structure. The details
of this structure are opaque to applications, but internally it is where all of
the TLS state is managed. To make sense of what is going on, it is necessary to
understand how the TLS protocol works at the record and handshake layers.

When a TLS connection is being started, the first communication consists of
handshake messages. The client sends the first message (a client hello), and
then the server replies (with a server hello), and so on. Because a server must
wait for a client and vice versa, this phase of a TLS connection is not
full-duplex. To save on memory, s2n uses a single stuffer for both incoming and
outgoing handshake messages and it is located as
``s2n_connection->handshake.io`` (which is a growable stuffer).

Borrowing another trick from functional programming, the state machine for
handling handshake messages is implemented using a table of function pointers,
located in `tls/s2n_handshake_io.c
<https://github.com/awslabs/s2n/blob/master/tls/s2n_handshake_io.c>`_.

.. literalinclude:: s2n_handshake_actions.c
   :language: c

The ``writer`` field indicates whether we expect a Client or a Server to write a
particular message type (or ``'B'`` for both in the case of an application data
message, but we haven't gotten to that yet). If s2n is acting as a server, then
it attempts to read client messages, if it's acting as a client it will try to
write it. To perform either operation it calls the relevant function pointer.
This way the state machine can be very short and simple: write a handshake
message out when we have one pending, and in the other direction read in data
until we have a fully-buffered handshake message before then calling the
relevant message parsing function.

Summary
    The effect of an action is implemented by the stored function pointer. 

(Implicit) Enabling Condition
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

+ Both server and client use the same table of actions for a given message but
  select different function pointer depending on its ``conn->mode`` (server or
  client)
+ How to choose next state/message refers to ``handshakes`` table. See Appendix_

  - Depend on which value of ``handshake_type`` set by
    ``s2n_conn_set_handshake_type``,
    ``handshakes[handshake_type]`` represents the sequence of messages that
    should happen.
  - Based on how many messages has been sent/received to decide next message to
    be processed
  - The macros, ``ACTIVE_STATE`` and ``ACTIVE_MESSAGE``, use
    ``(conn)->handshake.message_number`` to find out current message and
    indirectly represents current state
  - In short, the whole table exhaustively enumerates all allowed traces

+ An outer while loop in ``s2n_negotiate`` that will indirectly call
  ``s2n_advance_message`` to increment message number

  - Still figuring out how they wait to receive messages

TLS Standard
------------

.. todo::

    + Compare s2n implementation with standard
    + How does the global automaton, i.e., the specification look like?

Readings
--------

+ `SSL with Amazon Web Services <https://www.youtube.com/watch?v=8AODa_AazY4>`_

  - An overview video for SSL/TLS protocol
  - Technical details starting from
    `11:41 <https://www.youtube.com/watch?v=8AODa_AazY4&t=11m41s>`_

+ `The Transport Layer Security (TLS) Protocol Version 1.3
  <https://tools.ietf.org/html/draft-ietf-tls-tls13-28>`_

  - Draft for Internet Standard. Recently being approved.
  - `Sect. 2 <https://tools.ietf.org/html/draft-ietf-tls-tls13-28#section-2>`_
    for Protocol Overview with Message flow
  - `Sect. 4 <https://tools.ietf.org/html/draft-ietf-tls-tls13-28#section-4>`_
    for Handshake Protocol

+ :rfc:`5246` - The Transport Layer Security (TLS) Protocol Version 1.2

  - Proposed Standard
  - :rfc:`5246#section-7` for Handshake Protocols

+ More on https://github.com/awslabs/s2n/blob/master/docs/READING-LIST.md

Appendix
--------
.. literalinclude:: s2n_handshake_traces.c
   :language: c
