static struct s2n_handshake_action state_machine[] = {
    /* message_type_t           = {Record type             Message type               Writer   S2N_SERVER                S2N_CLIENT }  */
    [CLIENT_HELLO]              = {TLS_HANDSHAKE,          TLS_CLIENT_HELLO,             'C', {s2n_client_hello_recv, s2n_client_hello_send}}, 
    [SERVER_HELLO]              = {TLS_HANDSHAKE,          TLS_SERVER_HELLO,             'S', {s2n_server_hello_send, s2n_server_hello_recv}}, 
    [SERVER_NEW_SESSION_TICKET] = {TLS_HANDSHAKE,          TLS_SERVER_NEW_SESSION_TICKET,'S', {NULL, NULL}},     
    [SERVER_CERT]               = {TLS_HANDSHAKE,          TLS_SERVER_CERT,              'S', {s2n_server_cert_send, s2n_server_cert_recv}},
    [SERVER_CERT_STATUS]        = {TLS_HANDSHAKE,          TLS_SERVER_CERT_STATUS,       'S', {s2n_server_status_send, s2n_server_status_recv}},
    [SERVER_KEY]                = {TLS_HANDSHAKE,          TLS_SERVER_KEY,               'S', {s2n_server_key_send, s2n_server_key_recv}},
    [SERVER_CERT_REQ]           = {TLS_HANDSHAKE,          TLS_CLIENT_CERT_REQ,          'S', {s2n_client_cert_req_send, s2n_client_cert_req_recv}},
    [SERVER_HELLO_DONE]         = {TLS_HANDSHAKE,          TLS_SERVER_HELLO_DONE,        'S', {s2n_server_done_send, s2n_server_done_recv}}, 
    [CLIENT_CERT]               = {TLS_HANDSHAKE,          TLS_CLIENT_CERT,              'C', {s2n_client_cert_recv, s2n_client_cert_send}},
    [CLIENT_KEY]                = {TLS_HANDSHAKE,          TLS_CLIENT_KEY,               'C', {s2n_client_key_recv, s2n_client_key_send}},
    [CLIENT_CERT_VERIFY]        = {TLS_HANDSHAKE,          TLS_CLIENT_CERT_VERIFY,       'C', {s2n_client_cert_verify_recv, s2n_client_cert_verify_send}},
    [CLIENT_CHANGE_CIPHER_SPEC] = {TLS_CHANGE_CIPHER_SPEC, 0,                            'C', {s2n_client_ccs_recv, s2n_client_ccs_send}},
    [CLIENT_FINISHED]           = {TLS_HANDSHAKE,          TLS_CLIENT_FINISHED,          'C', {s2n_client_finished_recv, s2n_client_finished_send}},
    [SERVER_CHANGE_CIPHER_SPEC] = {TLS_CHANGE_CIPHER_SPEC, 0,                            'S', {s2n_server_ccs_send, s2n_server_ccs_recv}}, 
    [SERVER_FINISHED]           = {TLS_HANDSHAKE,          TLS_SERVER_FINISHED,          'S', {s2n_server_finished_send, s2n_server_finished_recv}},
    [APPLICATION_DATA]          = {TLS_APPLICATION_DATA,   0,                            'B', {NULL, NULL}}
};
