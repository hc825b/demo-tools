Specifications for SSL/TLS Protocol
===================================

Assumptions
-----------

+ Attacker can be an active network attacker, which means it has complete
  control over the network used to communicate between the parties [RFC3552].
+ TLS is susceptible to a variety of traffic analysis attacks based on observing
  the length and timing of encrypted packets [CLINIC] [HCJ16].
+ TLS does not have specific defenses against side-channel attacks

Informal Specs
--------------
Excerpt from `Introduction`_ and `Appendix E.1`_ in draft standard for TLS 1.3
especially the Handshake Protocol.

The primary goal of TLS is to provide a secure channel between two communicating
peers; the only requirement from the underlying transport is a reliable,
in-order, data stream. Specifically, the secure channel should provide the
following properties:

+ Authentication: The server side of the channel is always authenticated; the
  client side is optionally authenticated. Authentication can happen via
  asymmetric cryptography (e.g., RSA [RSA], ECDSA [ECDSA], EdDSA [RFC8032]) or a
  pre-shared key (PSK).

+ Confidentiality: Data sent over the channel after establishment is only
  visible to the endpoints. TLS does not hide the length of the data it
  transmits, though endpoints are able to pad TLS records in order to obscure
  lengths and improve protection against traffic analysis techniques.

+ Integrity: Data sent over the channel after establishment cannot be modified
  by attackers.

Functional Properties
~~~~~~~~~~~~~~~~~~~~~

The TLS handshake is an Authenticated Key Exchange (AKE) protocol which is
intended to provide both one-way authenticated (server-only) and mutually
authenticated (client and server) functionality. At the completion of the
handshake, each side outputs its view of the following values:

- A set of "session keys" (the various secrets derived from the master secret)
  from which can be derived a set of working keys.
- A set of cryptographic parameters (algorithms, etc.)
- The identities of the communicating parties.

Security Properties
~~~~~~~~~~~~~~~~~~~

Establishing the same session keys.
   The handshake needs to output the same set of session keys on both sides of
   the handshake, provided that it completes successfully on each endpoint (See
   [CK01]; defn 1, part 1).

Secrecy of the session keys.
   The shared session keys should be known only to the communicating parties and
   not to the attacker (See [CK01]; defn 1, part 2). Note that in a unilaterally
   authenticated connection, the attacker can establish its own session keys
   with the server, but those session keys are distinct from those established
   by the client.

Peer Authentication.
   The client's view of the peer identity should reflect the server's identity.
   If the client is authenticated, the server's view of the peer identity should
   match the client's identity.

Uniqueness of the session keys.
   Any two distinct handshakes should produce distinct, unrelated session keys.
   Individual session keys produced by a handshake should also be distinct and
   independent.

Downgrade protection.
   The cryptographic parameters should be the same on both sides and should be
   the same as if the peers had been communicating in the absence of an attack
   (See [BBFKZG16]; defns 8 and 9}).

Forward secret with respect to long-term keys.
   If the long-term keying material (in this case the signature keys in
   certificate- based authentication modes or the external/resumption PSK in PSK
   with (EC)DHE modes) is compromised after the handshake is complete, this does
   not compromise the security of the session key (See [DOW92]), as long as the
   session key itself has been erased. The forward secrecy property is not
   satisfied when PSK is used in the "psk_ke" PskKeyExchangeMode.

Key Compromise Impersonation (KCI) resistance.
   In a mutually- authenticated connection with certificates, compromising the
   long- term secret of one actor should not break that actor's authentication
   of their peer in the given connection (see [HGFS15]). For example, if a
   client's signature key is compromised, it should not be possible to
   impersonate arbitrary servers to that client in subsequent handshakes.

Protection of endpoint identities.
   The server's identity (certificate) should be protected against passive
   attackers. The client's identity should be protected against both passive
   and active attackers.

.. _Introduction: https://tools.ietf.org/html/draft-ietf-tls-tls13-28#section-1
.. _Appendix E.1: https://tools.ietf.org/html/draft-ietf-tls-tls13-28#appendix-E.1


Symbolic Analysis
-----------------

Assumptions
~~~~~~~~~~~

Based on Dolev-Yao Model

+ Computational/probabilistic security is not modeled. Assume encryption and
  decryption as black boxes.

+ **Passive Adversary**, the adversary can only eavesdrop but not insert
  messages

+ Side-channel attacks are not modeled

ProVerif formulation (π-calculus)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To understand ProVerif formulation, [Blanchet16]_ is a good reference.

Protocol Design
https://github.com/Inria-Prosecco/reftls/blob/master/pv/tls-lib.pvl

Queries
https://github.com/Inria-Prosecco/reftls/blob/master/pv/tls13-draft18-only.pv

.. todo::

    + Do they model **Active Adversary**?

Properties they verified as described in [SP17]_

Secrecy
    An adversary must not be able to recover the original (not encrypted) data.

Forward Secrecy
    Secrecy holds even if the long term keys of the client and server are given
    to the adversary after the session has been completed and the session keys
    are deleted.

Authentication
    If an application data is received over a session from a peer, then the peer
    must have sent the same data in a matching session.

Unique Channel Identifier
    If a client session and server session have the same identifier, then all
    other parameters in both sessions must match.

Replay Prevention
    Any application data sent over a session may be accepted at most once by the
    peer.

.. [Blanchet16] Bruno Blanchet. 2016. Modeling and Verifying Security Protocols with the Applied Pi Calculus and ProVerif. Found. Trends Priv. Secur. 1, 1-2 (October 2016), 1-135. DOI: https://doi.org/10.1561/3300000004

.. [SP17] Verified Models and Reference Implementations for the TLS 1.3 Standard
          Candidate, SP'17

Tamarin Formulation
~~~~~~~~~~~~~~~~~~~

See [CCS17]_.

.. [CCS17] A Comprehensive Symbolic Analysis of TLS 1.3, CCS'17
