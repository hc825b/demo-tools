Example Protocols
^^^^^^^^^^^^^^^^^

Asynchronous LCR algorithm for Leader Election
==============================================
Channel Automata are skipped for simplicity

.. literalinclude:: docs/AsyncLCR-no-channel.ioa
    :language: tioa
    :encoding: utf-8


CAN Bus Arbitration
===================

.. literalinclude:: docs/CAN-arbitration.ioa
    :language: tioa
    :encoding: utf-8

Needham-Schroeder protocol
==========================

.. literalinclude:: docs/NS.ioa
   :language: tioa

One Sided Secrecy Protocol
==========================

Components
----------

Here we provide OSS with each component modeled as an I/O automaton.

.. literalinclude:: docs/OSS.ioa
   :language: tioa
   :encoding: utf-8

Composition
-----------

Intuitively, we'd like the system automaton to simply be

.. code-block:: tioa

    System(I, R) = Protocol("I1", RoleI, I, R) ‖ Protocol("R1", RoleR, I, R)
                 ‖ Attacker("A") ‖ Environment

However, following the compatibility criteria, because ``send`` action is an
output action for all components except for ``Environment``, we actually have to
make sure the preconditions of ``send`` will be disjoint. This is guaranteed
because we assigned distinct ``PID`` to each component. We then duplicate the
``send`` input action of ``Environment`` with precondition corresponding to
different component instances. It is also possible to merge them into one
pre/eff block.

.. literalinclude:: docs/OSS-composed.ioa
   :language: tioa
   :encoding: utf-8

Possible issue
~~~~~~~~~~~~~~

The major concern of this approach is the role to agent instantiation we
skipped, that is, there can be possibly infinite instances of the same role
running on all agents. For example, Agent A1 can be both I and R in different
instances as follows.

.. code-block:: tioa

    System(A1, A2, A3, ...)
        = Protocol("I1", RoleI, A1, A2) ‖ Protocol("R1", RoleR, A1, A2)
        ‖ Protocol("I2", RoleI, A3, A1) ‖ Protocol("R2", RoleR, A3, A1)
        ‖ ...
        ‖ Attacker("A") ‖ Environment


It is obvious that each instance should have its ``send`` action, and
``Environment`` automaton will have possibly infinite duplicated ``send``
actions. For this particular example, we can easily merge the duplicated ``send``
actions for those instances of the same role. E.g., for role ``I``, we just need
to modify the precondition ``pid == "I1"`` to ``pid == "I1" ∨ pid == "I2"`` or
equivalently ``pid ∈ {"I1", "I2"}``. We might be able to extend this approach to
possibly infinite instances by partition the space of ``Id``.

Another issue is the bound on number of agents. This should've been addressed in
related works. May need to figure out a small bound to reduce effort for SMT
solver.

Also enumeration of role to agent combinations. We can check symmetry reduction
to explore only representative combinations.



Smart Grid Authentication Protocol
==================================

.. literalinclude:: docs/SGAS.ioa
   :language: tioa

