Examples built with CMake
=========================

These examples use CMake for automatic building. CMake_ is one of the most
popular build automation software for C/C++ language family. For these example,
we also want to show the effort to integrate each verification tool into CMake.

Requirements for building
-------------------------

+ CMake_ >= 2.8.8
+ C Compiler (Tested with Clang-3.9 and gcc-5.4.1)

.. _CMake: https://cmake.org/

.. toctree::
   :maxdepth: 1
   :caption: List of Examples

   tcp-echo/README
