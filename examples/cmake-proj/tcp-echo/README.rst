Echo TCP Server and Client
==========================

This example is excerpted from the book,
"`TCP/IP Sockets in C: Practical Guide for Programmers`__".
The original version can be found on `the book's website`_ shared and maintained
by the author, Prof. Donahoo.

We organize the code and add files to compile with CMake.

__ https://dl.acm.org/citation.cfm?id=2829296
.. _the book's website: http://cs.ecs.baylor.edu/~donahoo/practical/CSockets2


Service Spec
------------

Service specification in our definition should describe the observable behavior
of the system from outside. The external behavior of the system can be easily
described by the messages observed from outside of the system. In Echo TCP
system, we can observe that following messages should happen in the order
listed.

===============  ===========
Message          Description
===============  ===========
``Connect``      Client tries to connect to server
``Accept``       Server accepts the connection from the client
``SendData(m)``  Client sends a datagram ``m`` to server
``EchoData(m)``  Server echoes back exactly the same datagram ``m``
``Close``        Client terminates the connection
===============  ===========

We then can define an **Input/Output Automaton (IOA)** to formally specify the system.

.. literalinclude:: docs/Service.ioa
   :language: tioa

.. todo::

   Explain ``EchoStage`` actually is an infinite type because it stores the
   received datagram

Protocol Spec
-------------

.. literalinclude:: docs/Protocol.ioa
   :language: tioa

.. todo::

   Explain difference between parameters (e.g., ``mode``) and state variables
   (e.g. ``stage``)

Prove Protocol Implements Service
---------------------------------

Refinement Mapping
~~~~~~~~~~~~~~~~~~

.. math::

    s_S = f(s_P) =
        \left\{
        \begin{aligned}
            & s_P.server.stage, && \text{if }s_P.server.stage = s_P.client.stage \\
            & \mathtt{ERROR},   && \text{otherwise}
        \end{aligned}
        \right.

Refine Initial States
~~~~~~~~~~~~~~~~~~~~~

We have to prove all initial states of Protocol automaton would map to initial
states in Service automaton. Formally,

.. math::

    \forall s_P.\; Init_P(s_P) \Rightarrow Init_S(f(s_P))

It is trivial to prove because both server and client node in Protocol automaton
start with ``INIT`` stage. Therefore, any mapped state :math:`s_S = f((\mathtt{INIT,
INIT})) = \mathtt{INIT}` is obviously in the initial states of Service automaton.

Refine Transition Relation under Invariant
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We will have to prove following

.. math::

    \forall s_P, \alpha, s_P'.\;
          Inv_P(s_P) \land Trans_P(s_P, \alpha, s_P')
          \Rightarrow Trans_S(f(s_P), \alpha, f(s_P'))

We assume the invariant below when proving the refinement. The invariant will
be proven separately later.

.. math::
    Inv_P(s) \triangleq s.server.stage = s.client.stage

To prove this, we first observe that :math:`Inv_P` provides the guarantee that
the next state :math:`s_P` is still within the invariant. Therefore, we can
simplify the formula to

.. math::

    \forall s_P, \alpha, s_P'.\;
        &  Inv_P(s_P) \land Trans_P(s_P, \alpha, s_P') \\
        &  \Rightarrow Trans_S(s_P.server.stage, \alpha, s_P'.server.stage)

We can then derive that :math:`Trans_S(s_P.server.stage, \alpha,
s_P'.server.stage)` is implied by :math:`Trans_P(s_P, \alpha, s_P')` because we
designed the server mode of ``Node`` to be exactly the same as the Service
automaton. Therefore we conclude the proof.


Prove Code Implements Protocol
------------------------------

.. todo::

    + Setup different verification/testing tools to do conformance checking
