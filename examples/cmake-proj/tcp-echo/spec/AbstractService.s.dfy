include "Types.i.dfy"

module AbstractServiceEcho_s {
import opened Types_i

datatype ServiceState' = ServiceState'(stage:EchoStage)

type ServiceState = ServiceState'

predicate Enabled(act:EchoAction, s:ServiceState)
{
    // TODO Need to know if choosing action is nondeterministic when
    // preconditions are overlapping
    match act {
        case Connect     => s.stage == INIT
        case Accept      => s.stage == CONNECTING
        case SendData(_) => s.stage == ACCEPTED
        case EchoData(m) => s.stage == DATASENT(m)
        case Close       => s.stage == DATAECHOED
    }
}

function Trans(act:EchoAction, s:ServiceState) : ServiceState
{
    match act {
        case Connect     => s.(stage := CONNECTING)
        case Accept      => s.(stage := ACCEPTED)
        case SendData(m) => s.(stage := DATASENT(m))
        case EchoData(_) => s.(stage := DATAECHOED)
        case Close       => s.(stage := CLOSED)
    }
}

predicate Service_Init(s:ServiceState)
{
    s.stage == INIT
}

predicate Service_Transition(s:ServiceState, act:EchoAction, s':ServiceState)
{
    // NOTE If the action is enabled, then it must be taken.
    if Enabled(act, s) then
        s' == Trans(act, s)
    else
        s' == s
}

predicate Service_Next(s:ServiceState, s':ServiceState)
{
    exists act :: Service_Transition(s, act, s')
}

} // end of module AbstractServiceEcho_s
