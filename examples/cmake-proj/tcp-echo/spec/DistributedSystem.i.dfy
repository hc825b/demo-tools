include "Types.i.dfy"
include "Node.i.dfy"

module DistributedSystem_i {
    import opened Types_i
    import opened Protocol_Node_i

datatype LS_State = LS_State(
        server : Node,
        client : Node
    )

predicate LS_Init(s: LS_State) {
       Node_Init(s.server, SERVER)
    && Node_Init(s.client, CLIENT)
}

predicate LS_Transition(s: LS_State, act: EchoAction, s':LS_State) {
       Node_Transition(s.server, act, s'.server)
    && Node_Transition(s.client, act, s'.client)
}

} // end module DistributedSystem_i
