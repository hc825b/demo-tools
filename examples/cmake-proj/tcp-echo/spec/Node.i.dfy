include "Types.i.dfy"

module Protocol_Node_i {
import opened Types_i

datatype Mode = SERVER | CLIENT

datatype Node = Node(stage: EchoStage, mode: Mode)

predicate Enabled(act:EchoAction, s:Node)
{
    match act {
        case Connect     => s.stage == INIT
        case Accept      => s.stage == CONNECTING
        case SendData(_) => s.stage == ACCEPTED
        case EchoData(m) => s.stage == DATASENT(m)
        case Close       => s.stage == DATAECHOED
    }
}

function Trans(act:EchoAction, s:Node) : Node
{
    match act {
        case Connect    =>
            match s.mode {
                case SERVER => s.(stage := CONNECTING)
                case CLIENT => s.(stage := CONNECTING)
            }
        case Accept     =>
            match s.mode {
                case SERVER => s.(stage := ACCEPTED)
                case CLIENT => s.(stage := ACCEPTED)
            }
        case SendData(m) =>
            match s.mode {
                case SERVER => s.(stage := DATASENT(m))
                case CLIENT => s.(stage := DATASENT(m))
            }
        case EchoData(_) =>
            match s.mode {
                case SERVER => s.(stage := DATAECHOED)
                case CLIENT => s.(stage := DATAECHOED)
            }
        case Close      =>
            match s.mode {
                case SERVER => s.(stage := CLOSED)
                case CLIENT => s.(stage := CLOSED)
            }
    }
}

predicate Node_Init(s: Node, initMode: Mode) {
    s.mode == initMode && s.stage == INIT
}

predicate Node_Transition(s: Node, act:EchoAction, s': Node) {
    // NOTE If the action is enabled, then it must be taken.
    if Enabled(act, s) then
        s' == Trans(act, s)
    else
        s' == s
}

} // end module Protocol_Node_i
