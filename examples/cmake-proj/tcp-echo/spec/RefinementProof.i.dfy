include "Types.i.dfy"
include "DistributedSystem.i.dfy"
include "AbstractService.s.dfy"

module Refinement_i {
    import opened Types_i
    import opened DistributedSystem_i
    import opened AbstractServiceEcho_s

    type Excution<State(==)> = seq<(State, EchoAction)>
    type Trace = seq<EchoAction>

    type LS_Execution = Excution<LS_State>
    type Service_Execution = Excution<ServiceState>

    function AbstractifyLS_State(ls:LS_State) : ServiceState
    {
        if(ls.server.stage == ls.client.stage) then
            ServiceState'(ls.server.stage)
        else
            ServiceState'(ERROR)
    }

    lemma textbook_RefinementProof_Init()
        ensures forall ls :: LS_Init(ls) ==> Service_Init(AbstractifyLS_State(ls))
    {}

    lemma textbook_RefinementProof_TransitionWithinInvariant()
        ensures
            forall ls, act, ls' :: LS_Invariant(ls) && LS_Transition(ls, act, ls')
        ==> Service_Transition(AbstractifyLS_State(ls), act, AbstractifyLS_State(ls'))
    {}

    predicate LS_Invariant(ls: LS_State)
    {
        ls.server.stage == ls.client.stage
    }

    lemma textbook_InvariantProof_Init()
        ensures forall ls : LS_State :: LS_Init(ls) ==> LS_Invariant(ls)
    {}

    lemma textbook_InvariantProof_Inductive()
        ensures forall ls, act, ls' ::
            LS_Invariant(ls) && LS_Transition(ls, act, ls') ==> LS_Invariant(ls')
    {}

    // From IronFleet
    predicate IsValidLS_Execution(ex: LS_Execution)
    {
           |ex| > 0
        && LS_Init(ex[0].0)
        && (forall i {:exigger LS_Transition(ex[i].0, ex[i].1, ex[i+1].0)} ::
               0 <= i < |ex| - 1 ==> LS_Transition(ex[i].0, ex[i].1, ex[i+1].0))
    }

    lemma lemma_InitRefines(ls: LS_State)
        requires LS_Init(ls)
        ensures  Service_Init(AbstractifyLS_State(ls))
    {}

    lemma lemma_TransitionRefinesUnderInvariant(ls: LS_State, act: EchoAction, ls': LS_State)
        requires LS_Invariant(ls)
        requires LS_Transition(ls, act, ls')
        ensures  Service_Transition(AbstractifyLS_State(ls), act, AbstractifyLS_State(ls'))
    {}

    lemma lemma_InvariantInit(ls: LS_State)
        requires LS_Init(ls)
        ensures  LS_Invariant(ls)
    {}

    lemma lemma_InvariantInductive(ls: LS_State, act: EchoAction, ls': LS_State)
        requires LS_Invariant(ls) && LS_Transition(ls, act, ls')
        ensures  LS_Invariant(ls')
    {}

    lemma lemma_ExecutionsInvariant(ex: LS_Execution, i: nat)
        requires IsValidLS_Execution(ex)
        requires i < |ex| - 1;
        ensures  LS_Invariant(ex[i].0) && LS_Invariant(ex[i+1].0)
    {
        if (i == 0) {
            lemma_InvariantInductive(ex[i].0, ex[i].1, ex[i+1].0);
        } else {
            // Inductive Hypothesis
            lemma_ExecutionsInvariant(ex, i - 1);
            lemma_InvariantInductive(ex[i].0, ex[i].1, ex[i+1].0);
        }
    }

    lemma lemma_ExecutionsRefines(ex: LS_Execution, i: nat)
        requires IsValidLS_Execution(ex)
        requires i < |ex| - 1
        ensures  Service_Transition(AbstractifyLS_State(ex[i].0), ex[i].1, AbstractifyLS_State(ex[i+1].0))
    {
        lemma_ExecutionsInvariant(ex, i);
        lemma_TransitionRefinesUnderInvariant(ex[i].0, ex[i].1, ex[i+1].0);
    }
}
