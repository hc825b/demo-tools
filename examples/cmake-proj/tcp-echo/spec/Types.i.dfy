// include "../../Common/Framework/Environment.s.dfy"
// include "../../Common/Native/Io.s.dfy"

module Types_i {
// import opened Environment_s
// import opened Native__Io_s


datatype EchoStage = INIT | CONNECTING | ACCEPTED | DATASENT(data: string) | DATAECHOED | CLOSED | ERROR
datatype EchoAction = Connect | Accept | SendData(data_s: string) | EchoData(data_e: string) | Close

/*
type EchoEnvironment = LEnvironment<EndPoint, EchoAction>
type EchoPacket = LPacket<EndPoint, EchoAction>
type EchoIo = LIoOp<EndPoint, EchoAction>
*/
}
