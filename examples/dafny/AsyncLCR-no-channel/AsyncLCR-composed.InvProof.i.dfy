include "AsyncLCR.i.dfy"
include "AsyncLCR-composed.i.dfy"

module AsyncLCR_composed_InvProof {
import opened Types
import opened AsyncLCR_composed

predicate Axioms() {
    u0 != u1 && u1 != u2 && u2 != u0
}

function max(a: UID, b: UID): UID
{
    if a >= b then a else b
}

function max3(x0: UID, x1: UID, x2: UID): UID
{
    max(x0, max(x1, x2))
}

function i_max(): UID
    requires Axioms()
{
    if u0 == max3(u0, u1, u2) then 0
    else if u1 == max3(u0, u1, u2) then 1
    else assert u2 == max3(u0, u1, u2); 2
}

predicate Invariant(s: State)
    requires Axioms()
{
    (i_max() != 0 ==> (s.p0.status == unknown))
 && (i_max() != 1 ==> (s.p1.status == unknown))
 && (i_max() != 2 ==> (s.p2.status == unknown))
 && AuxInvariant(s)
}

predicate between(lower: UID, x: UID, upper: UID)
{
    if lower <= upper then
        (lower <= x < upper)
    else
        (x >= lower || x < upper)
}

predicate AuxInvariant(s: State)
    requires Axioms()
{
    true
 && ((0 != i_max() && between(i_max(), 0, 0)) ==> u0 !in s.p0.q)
 && ((0 != i_max() && between(i_max(), 1, 0)) ==> u0 !in s.p1.q)
 && ((0 != i_max() && between(i_max(), 2, 0)) ==> u0 !in s.p2.q)
 && ((1 != i_max() && between(i_max(), 0, 1)) ==> u1 !in s.p0.q)
 && ((1 != i_max() && between(i_max(), 1, 1)) ==> u1 !in s.p1.q)
 && ((1 != i_max() && between(i_max(), 2, 1)) ==> u1 !in s.p2.q)
 && ((2 != i_max() && between(i_max(), 0, 2)) ==> u2 !in s.p0.q)
 && ((2 != i_max() && between(i_max(), 1, 2)) ==> u2 !in s.p1.q)
 && ((2 != i_max() && between(i_max(), 2, 2)) ==> u2 !in s.p2.q)

}

lemma BMC_Invariant(
                   s00: State
    , a01: Action, s01: State
    , a02: Action, s02: State
    , a03: Action, s03: State
    , a04: Action, s04: State
)
    requires Axioms()
    requires Initial(s00)
    ensures  Invariant(s00)
    requires Transition(s00, a01, s01)
    ensures  Invariant(s01)
    requires Transition(s01, a02, s02)
    ensures  Invariant(s02)
    requires Transition(s02, a03, s03)
    ensures  Invariant(s03)
    requires Transition(s03, a04, s04)
    ensures  Invariant(s04)
{}

lemma Inductive_Invariant(
                   s00: State
    , a01: Action, s01: State
)
    requires Axioms()
    requires Invariant(s00) && Transition(s00, a01, s01)
    ensures  Invariant(s01)
{}

} // end module
