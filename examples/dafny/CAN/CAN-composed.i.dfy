include "CAN_Types.s.dfy"

include "CAN_NodeSeq.i.dfy"
include "CAN_Bus.i.dfy"

module CAN_Composed {

import opened CAN_Types

import Bus = CAN_Bus
import NodeSeq = CAN_NodeSeq

datatype State = State(bus : Bus.State, nseq : NodeSeq.State)

predicate Initial(s : State) {
    Bus.Initial(s.bus)
 && NodeSeq.Initial(s.nseq)
}

predicate Input(act : Action) {
    (Bus.Input(act) || NodeSeq.Input(act))
 && ! Output(act)
}

predicate Output(act : Action) {
    Bus.Output(act) || NodeSeq.Output(act)
}

predicate Internal(act : Action) {
    Bus.Internal(act) || NodeSeq.Internal(act)
}

lemma Action_Set_Disjoint(act: Action)
    ensures !(Output(act) && Input(act))
         && !(Output(act) && Internal(act))
         && !(Internal(act) && Input(act))
{}

predicate External(act: Action) {
    Input(act) || Output(act)
}

predicate Signature(act : Action) {
    Internal(act) || External(act)
}

predicate Transition(s: State, act: Action, s': State) {
    (if Bus.Signature(act)
     then Bus.Transition(s.bus, act, s'.bus)
     else s'.bus == s.bus)
 && (if NodeSeq.Signature(act)
     then NodeSeq.Transition(s.nseq, act, s'.nseq)
     else s'.nseq == s.nseq)
}

} // End module CAN_Composed

module CAN_Proof {

import opened CAN_Types
import opened CAN_Node_Types
import opened CAN_Composed

import NodeSeq = CAN_NodeSeq

function min_node(ns : seq<NodeSeq.NodeState>) : int
    requires |ns| >= 1
    ensures  0 <= min_node(ns) < |ns|
    ensures  forall i | 0 <= i < |ns| :: ns[min_node(ns)].arb <= ns[i].arb

predicate prop_BusAccessMethod(s: State)
{
    var ns := s.nseq.nodes;
    |ns| >= 1
 && var min := min_node(ns);
    ns[min].transmit
 &&(s.nseq.pos < 0 ==>
        forall i: nat | i < |ns| ::
            ns[i].transmit ==> ns[i].arb == ns[min].arb
   )
}

lemma proof_BAM_Init(
                   s00: State
    , a01: Action, s01: State
    , a02: Action, s02: State
    , a03: Action, s03: State
    , a04: Action, s04: State
    , a05: Action, s05: State
    , a06: Action, s06: State
    , a07: Action, s07: State
    , a08: Action, s08: State
    , a09: Action, s09: State
    , a10: Action, s10: State
    , a11: Action, s11: State
    , a12: Action, s12: State
    , a13: Action, s13: State
    , a14: Action, s14: State
    , a15: Action, s15: State
    , a16: Action, s16: State
    , a17: Action, s17: State
    , a18: Action, s18: State
    , a19: Action, s19: State
    , a20: Action, s20: State
    , a21: Action, s21: State
    , a22: Action, s22: State
)
    requires Initial(s00)
    ensures  prop_BusAccessMethod(s00)
    requires a01.send? && Transition(s00, a01, s01)
    ensures  prop_BusAccessMethod(s01)
    requires a02.recv? && Transition(s01, a02, s02)
    ensures  prop_BusAccessMethod(s02)
    requires a03.send? && Transition(s02, a03, s03)
    ensures  prop_BusAccessMethod(s03)
    requires a04.recv? && Transition(s03, a04, s04)
    ensures  prop_BusAccessMethod(s04)
    requires a05.send? && Transition(s04, a05, s05)
    ensures  prop_BusAccessMethod(s05)
    requires a06.recv? && Transition(s05, a06, s06)
    ensures  prop_BusAccessMethod(s06)
    requires a07.send? && Transition(s06, a07, s07)
    ensures  prop_BusAccessMethod(s07)
    requires a08.recv? && Transition(s07, a08, s08)
    ensures  prop_BusAccessMethod(s08)
    requires a09.send? && Transition(s08, a09, s09)
    ensures  prop_BusAccessMethod(s09)
    requires a10.recv? && Transition(s09, a10, s10)
    ensures  prop_BusAccessMethod(s10)
    requires a11.send? && Transition(s10, a11, s11)
    ensures  prop_BusAccessMethod(s11)
    requires a12.recv? && Transition(s11, a12, s12)
    ensures  prop_BusAccessMethod(s12)
    requires a13.send? && Transition(s12, a13, s13)
    ensures  prop_BusAccessMethod(s13)
    requires a14.recv? && Transition(s13, a14, s14)
    ensures  prop_BusAccessMethod(s14)
    requires a15.send? && Transition(s14, a15, s15)
    ensures  prop_BusAccessMethod(s15)
    requires a16.recv? && Transition(s15, a16, s16)
    ensures  prop_BusAccessMethod(s16)
    requires a17.send? && Transition(s16, a17, s17)
    ensures  prop_BusAccessMethod(s17)
    requires a18.recv? && Transition(s17, a18, s18)
    ensures  prop_BusAccessMethod(s18)
    requires a19.send? && Transition(s18, a19, s19)
    ensures  prop_BusAccessMethod(s19)
    requires a20.recv? && Transition(s19, a20, s20)
    ensures  prop_BusAccessMethod(s20)
    requires a21.send? && Transition(s20, a21, s21)
    ensures  prop_BusAccessMethod(s21)
    requires a22.recv? && Transition(s21, a22, s22)
    ensures  prop_BusAccessMethod(s22)
{}

lemma proof_BAM_Inductive(
                   s00: State
    , a01: Action, s01: State
    , a02: Action, s02: State
    , a03: Action, s03: State
    , a04: Action, s04: State
    , a05: Action, s05: State
    , a06: Action, s06: State
    , a07: Action, s07: State
    , a08: Action, s08: State
    , a09: Action, s09: State
    , a10: Action, s10: State
    , a11: Action, s11: State
    , a12: Action, s12: State
    , a13: Action, s13: State
    , a14: Action, s14: State
    , a15: Action, s15: State
    , a16: Action, s16: State
    , a17: Action, s17: State
    , a18: Action, s18: State
    , a19: Action, s19: State
    , a20: Action, s20: State
    , a21: Action, s21: State
    , a22: Action, s22: State
    , a23: Action, s23: State
    , a24: Action, s24: State
)
    requires prop_BusAccessMethod(s00)
    requires a01.send? && Transition(s00, a01, s01)
    requires prop_BusAccessMethod(s01)
    requires a02.recv? && Transition(s01, a02, s02)
    requires prop_BusAccessMethod(s02)
    requires a03.send? && Transition(s02, a03, s03)
    requires prop_BusAccessMethod(s03)
    requires a04.recv? && Transition(s03, a04, s04)
    requires prop_BusAccessMethod(s04)
    requires a05.send? && Transition(s04, a05, s05)
    requires prop_BusAccessMethod(s05)
    requires a06.recv? && Transition(s05, a06, s06)
    requires prop_BusAccessMethod(s06)
    requires a07.send? && Transition(s06, a07, s07)
    requires prop_BusAccessMethod(s07)
    requires a08.recv? && Transition(s07, a08, s08)
    requires prop_BusAccessMethod(s08)
    requires a09.send? && Transition(s08, a09, s09)
    requires prop_BusAccessMethod(s09)
    requires a10.recv? && Transition(s09, a10, s10)
    requires prop_BusAccessMethod(s10)
    requires a11.send? && Transition(s10, a11, s11)
    requires prop_BusAccessMethod(s11)
    requires a12.recv? && Transition(s11, a12, s12)
    requires prop_BusAccessMethod(s12)
    requires a13.send? && Transition(s12, a13, s13)
    requires prop_BusAccessMethod(s13)
    requires a14.recv? && Transition(s13, a14, s14)
    requires prop_BusAccessMethod(s14)
    requires a15.send? && Transition(s14, a15, s15)
    requires prop_BusAccessMethod(s15)
    requires a16.recv? && Transition(s15, a16, s16)
    requires prop_BusAccessMethod(s16)
    requires a17.send? && Transition(s16, a17, s17)
    requires prop_BusAccessMethod(s17)
    requires a18.recv? && Transition(s17, a18, s18)
    requires prop_BusAccessMethod(s18)
    requires a19.send? && Transition(s18, a19, s19)
    requires prop_BusAccessMethod(s19)
    requires a20.recv? && Transition(s19, a20, s20)
    requires prop_BusAccessMethod(s20)
    requires a21.send? && Transition(s20, a21, s21)
    requires prop_BusAccessMethod(s21)
    requires a22.recv? && Transition(s21, a22, s22)
    requires prop_BusAccessMethod(s22)
    requires a23.send? && Transition(s22, a23, s23)
    ensures  prop_BusAccessMethod(s23)
    requires a24.recv? && Transition(s23, a24, s24)
    ensures  prop_BusAccessMethod(s24)
{}
} // End module CAN_Proof
