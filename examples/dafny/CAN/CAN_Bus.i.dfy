include "CAN_Types.s.dfy"

module CAN_Bus {

import opened CAN_Types

datatype State = State(bus: Bit)

predicate Input(act: Action) {
    act.send?
}

predicate Output(act: Action) {
    act.recv?
}

predicate Internal(act: Action) {
    false
}

lemma Action_Set_Disjoint(act: Action)
    ensures !(Output(act) && Input(act))
         && !(Output(act) && Internal(act))
         && !(Internal(act) && Input(act))
{}

predicate External(act: Action) {
    Input(act) || Output(act)
}

predicate Signature(act : Action) {
    Internal(act) || External(act)
}

predicate Initial(s: State) {
    s.bus == RECESSIVE
}

predicate pre_send(act: Action, s: State) {
    Input(act) && act.send?
}

function  eff_send(s: State, act: Action) : State
    requires pre_send(act, s)
{
    var s: State := s.(bus :=
        if forall i : nat | i < |act.msgs| :: act.msgs[i] == RECESSIVE
        then RECESSIVE
        else DOMINANT
    ); s
}

predicate pre_recv(act: Action, s: State) {
    Output(act) && act.recv? && act == recv(s.bus)
}

function  eff_recv(s: State, act: Action) : State
    requires pre_recv(act, s)
{
    s
}

predicate Transition(s : State, act : Action, s' : State) {
    (pre_send(act, s) && eff_send(s, act) == s')
 || (pre_recv(act, s) && eff_recv(s, act) == s')
}

lemma Input_Enabled(act: Action, s: State)
    requires Input(act)
    ensures  exists s': State :: Transition(s, act, s')
{
    var s': State :| Transition(s, act, s');
}

} // End module CAN_Bus
