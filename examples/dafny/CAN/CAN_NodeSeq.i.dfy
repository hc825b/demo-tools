include "CAN_Types.s.dfy"

module CAN_Node_Types {

import opened CAN_Types

newtype BitPos = pos : int | -1 <= pos < 11
function GetBit(vec: bv11, pos: BitPos) : Bit
{
    if pos < 0 then
        DOMINANT
    else
        var bit := ((vec >> pos) & 0x1);
        assert 0 <= bit < 2;
        bit as Bit
}

}

module CAN_NodeSeq {

import opened CAN_Types
import opened CAN_Node_Types

datatype NodeState = NodeState(arb: bv11, transmit: bool)
datatype State = State(pos: BitPos, nodes: seq<NodeState>)

predicate Input(act : Action) {
    act.recv?
}

predicate Output(act : Action) {
    act.send?
}

predicate Internal(act: Action) {
    false
}

lemma Action_Set_Disjoint(act: Action)
    ensures !(Output(act) && Input(act))
         && !(Output(act) && Internal(act))
         && !(Internal(act) && Input(act))
{}

predicate External(act: Action) {
    Input(act) || Output(act)
}

predicate Signature(act : Action) {
    Internal(act) || External(act)
}

/*
predicate UniqueArbitration(nodes: seq<NodeState>)
{
    forall i, j | 0 <= i < j < |nodes| :: nodes[i].arb != nodes[j].arb
}
*/

predicate Initial(s: State) {
    s.pos == 10 && |s.nodes| >= 1
 && forall i: nat | i < |s.nodes| :: s.nodes[i].transmit
// && UniqueArbitration(s.nodes)
}

predicate pre_send(act: Action, s: State) {
    Output(act) && act.send?
 && |act.msgs| == |s.nodes|
 && forall i: nat | i < |s.nodes| ::
                    if s.nodes[i].transmit
                    then act.msgs[i] == GetBit(s.nodes[i].arb, s.pos)
                    else act.msgs[i] == RECESSIVE
}

function  eff_send(s: State, act: Action) : State
    requires pre_send(act, s)
{
    s
}

predicate pre_recv(act: Action, s: State) {
    Input(act) && act.recv?
}

predicate rel_eff_recv(s: State, act: Action, s': State)
    requires pre_recv(act, s)
    ensures rel_eff_recv(s, act, s')
        ==> (|s'.nodes| == |s.nodes|
          && forall i: nat | i < |s.nodes| :: s.nodes[i].arb == s'.nodes[i].arb)
{
    if s.pos >= 0 then
        |s'.nodes| == |s.nodes|
     && forall i: nat | i < |s.nodes| ::
         s'.nodes[i] ==(if s.nodes[i].transmit
                        && act.bus != GetBit(s.nodes[i].arb, s.pos)
                        then if act.bus == DOMINANT
                             then s.nodes[i].(transmit := false) // Lose arbitration
                             else s.nodes[i].(transmit := false) // TODO error state
                        else s.nodes[i])
     && s'.pos == s.pos - 1
    else
        s' == s
}

predicate Transition(s : State, act : Action, s' : State) {
    (pre_send(act, s) && eff_send(s, act) == s')
 || (pre_recv(act, s) && rel_eff_recv(s, act, s'))
}

lemma Input_Enabled(act: Action, s: State)
    requires Input(act)
    ensures  exists s': State :: Transition(s, act, s')
{
    var s': State :| Transition(s, act, s');
}


} // End module CAN_NodeSeq
