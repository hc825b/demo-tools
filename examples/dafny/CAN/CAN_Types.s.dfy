module CAN_Types {

type Bit = bv1
const RECESSIVE : Bit := 0x1
const DOMINANT  : Bit := 0x0

datatype Action = send(msgs: seq<Bit>) | recv(bus: Bit)

}

