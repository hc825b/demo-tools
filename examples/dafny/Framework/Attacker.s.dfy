include "Types.s.dfy"

module Attacker {

import opened Types

datatype State = State(know : Knowledge)

predicate Initial(s : State) {
    s.know == { Fresh(0)
              , AgentTerm(A), KeyTerm(PK(A))
              , AgentTerm(B), KeyTerm(PK(B))
              , AgentTerm(E), KeyTerm(PK(E)), KeyTerm(SK(E))
              }
}

predicate ValidAction(act : Action) {
    InputAction(act) || OutputAction(act)
}

predicate InputAction(act : Action) {
    act.recv?
}

predicate OutputAction(act : Action) {
    act.send? && act.rid == 0
}

predicate pre_send(act: Action, s: State)
{
    act.send? && var send(rid, aS, aR, term) := act;
    rid == 0 && Entail(s.know, term)
}

function  eff_send(s: State, act: Action) : State
    requires pre_send(act, s) 
{
    s // Sending does not change attacker's state
}

predicate pre_recv(act: Action, s: State)
{
    act.recv?
}

function  eff_recv(s:State, act: Action) : State
    requires pre_recv(act, s)
{
    var recv(_, _, term) := act;
    s.(know := s.know + decomposeTerm(s.know, term))
}

predicate Transition(s : State, act : Action, s' : State) {
    (pre_send(act, s) && eff_send(s, act) == s')
 || (pre_recv(act, s) && eff_recv(s, act) == s')
}

} // End module Attacker
