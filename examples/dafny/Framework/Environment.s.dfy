include "Types.s.dfy"

module Environment {

import opened Types

type Message = (Agent, Agent, RunTerm)

datatype State = State(q : seq<Message>)

predicate Initial(s : State) {
    s.q == []
}

predicate ValidAction(act : Action)
    ensures ValidAction(act) // All actions is valid for Environment
{
    InputAction(act) || OutputAction(act)
}

predicate InputAction(act : Action) {
    act.send?
}

predicate OutputAction(act : Action) {
    act.recv?
}

predicate pre_send(act : Action, s : State) {
    act.send?
}

function  eff_send(s: State, act: Action) : State
    requires pre_send(act, s) 
{
    var send(_, aS, aR, term) := act;
    s.(q := s.q + [(aS, aR, term)])
}

predicate pre_recv(act: Action, s: State) {
    act.recv? && var recv(aS, aR, term) := act;
    s.q != [] && s.q[0] == (aS, aR, term)
}

function  eff_recv(s:State, act: Action) : State
    requires pre_recv(act, s)
{
    s.(q := s.q[1..])
}

predicate Transition(s : State, act : Action, s' : State) {
    (pre_send(act, s) && eff_send(s, act) == s')
 || (pre_recv(act, s) && eff_recv(s, act) == s')
}

} // End module Environment
