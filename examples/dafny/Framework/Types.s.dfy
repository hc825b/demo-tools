module Types {

type RunId = nat
type Nonce = int

datatype Role  = RoleI | RoleR

//datatype Agent = A | B | E
type Agent = char
const A : Agent := 'A'
const B : Agent := 'B'
const E : Agent := 'E'

datatype Key = PK(Agent) | SK(Agent)

function Inverse(k : Key) : Key
{
    match k {
    case PK(a) => SK(a)
    case SK(a) => PK(a)
    }
}

datatype RunTerm = Fresh(Nonce)
                 | AgentTerm(Agent) | Pair(RunTerm, RunTerm)
                 | KeyTerm(Key)
                 | Enc(msg:RunTerm, key:Key)

type Knowledge = set<RunTerm>

datatype Action = send(rid : RunId, Agent, Agent, term : RunTerm)
                | recv(Agent, Agent, term : RunTerm)

predicate Entail(know : Knowledge, term : RunTerm) {
    // TODO implement simple term inference
    (term in know)
 || (if term.Pair? then
        var Pair(t1, t2) := term;
        t1 in know && t2 in know
    else
        term.Enc? && var Enc(msg, key) := term;
        KeyTerm(key) in know && msg in know
    )
}

function decomposeTerm(know : Knowledge, term : RunTerm) : Knowledge
    decreases term
{
    if term.Pair? then
        var Pair(t1, t2) := term;
        {term, t1, t2}
    else if term.Enc? && KeyTerm(Inverse(term.key)) in know then
        {term, term.msg}
    else
        {term}
}


predicate Match(value : RunTerm, pattern : RunTerm) {
    // TODO implement simple unification algorithm
    value == pattern
}

} // End module

