include "../Framework/Types.s.dfy"
include "../Framework/Environment.s.dfy"
include "../Framework/Attacker.s.dfy"

module Hello1 {

import opened Types

newtype Loc = pc : nat | 0 <= pc < 3

datatype State = State(pc : Loc)

predicate Initial(s : State) {
    s == State(0)
}

predicate ValidAction(para : Parameter, act : Action) {
    match act {
        case send(rid, _, _, _) => para.rid == rid
        case recv(_, _, _) => true
    }
}

predicate pre_send(para : Parameter, act : Action, s : State) {
    act.send? && var send(rid, aS, aR, term) := act; para.rid == rid
 && match para.role {
        case RoleI =>
            para.i == aS && para.r == aR && s.pc == 0
         && term == Enc(Data("Hello"), SK(AgentTerm(para.i)))
        case RoleR =>
            para.i == aR && para.r == aS && s.pc == 1
         && term == Enc(Data("Hello"), SK(AgentTerm(para.r)))
    }
}

function  eff_send(para : Parameter, s : State, act : Action) : State
    requires pre_send(para, act, s)
{
    s.(pc := s.pc + 1)
}

predicate pre_recv(para : Parameter, act : Action, s : State) {
    act.recv?
}

function  eff_recv(para : Parameter, s : State, act : Action) : State
    requires pre_recv(para, act, s)
{
    var recv(aS, aR, term) := act;
    match para.role {
        case RoleI =>
            if para.i == aR && para.r == aS && s.pc == 1
            && Match(term, Enc(Data("Hello"), SK(AgentTerm(para.r)))) then
                s.(pc := 2)
            else
                s
        case RoleR =>
            if para.i == aS && para.r == aR && s.pc == 0
            && Match(term, Enc(Data("Hello"), SK(AgentTerm(para.i)))) then
                s.(pc := 1)
            else
                s
    }
}

predicate Transition(para : Parameter, s : State, act : Action, s' : State) {
    (pre_send(para, act, s) && eff_send(para, s, act) == s')
 || (pre_recv(para, act, s) && eff_recv(para, s, act) == s')
}

} // End module Hello1


module Hello1_Composed {

import opened Types

import Env = Environment
import Att = Attacker
import P = Hello1
datatype SysParameter = SysParameter(i1 : Parameter, r1 : Parameter)
datatype State = State(i1 : P.State, r1 : P.State, att : Att.State, env : Env.State)

predicate Initial(s : State) {
    Env.Initial(s.env)
 && Att.Initial(s.att)
 && P.Initial(s.i1)
 && P.Initial(s.r1)
}

predicate Transition(para : SysParameter, s: State, act: Action, s': State) {
    // TODO figure out when does act belongs to the Action set of a component
    (if Env.ValidAction(act) then Env.Transition(s.env, act, s'.env) else s'.env == s.env)
 && (if Att.ValidAction(act) then Att.Transition(s.att, act, s'.att) else s'.att == s.att)
 && (if P.ValidAction(para.i1, act) then P.Transition(para.i1, s.i1, act, s'.i1) else s'.i1 == s.i1)
 && (if P.ValidAction(para.r1, act) then P.Transition(para.r1, s.r1, act, s'.r1) else s'.r1 == s.r1)
}

} // End module Hello1_Composed

module Hello1_Proof {

import opened Types
import opened Hello1_Composed

}
