include "MutEx01.i.dfy"

module MutEx01_InvProof {
import opened Types
import opened MutEx01

predicate Invariant(s: State) {
    !(s.reg_0 == crit && s.reg_1 == crit)
}

lemma BMC_Invariant(
                  s0: State
    , a1: Action, s1: State
)
    requires Initial(s0)
    ensures  Invariant(s0)
{}

lemma Inductive_Invariant(
                  s0: State
    , a1: Action, s1: State
    , a2: Action, s2: State
)
    requires Invariant(s0)
    requires Transition(s0, a1, s1)
    requires Invariant(s1)
    requires Transition(s1, a2, s2)
    ensures  Invariant(s2)
{}


} 
