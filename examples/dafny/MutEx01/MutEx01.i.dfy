module Types{
datatype region = try | crit | exit | rem

datatype Action = try_0 | try_1
                | crit_0 | crit_1
                | exit_0 | exit_1
                | rem_0 | rem_1
}

module MutEx01 {

import opened Types

datatype State = State(reg_0: region, reg_1: region)

predicate Input(act: Action) {
    act.try_0?
 || act.try_1?
 || act.exit_0?
 || act.exit_1?
}

predicate Output(act: Action) {
    act.crit_0?
 || act.crit_1?
 || act.rem_0?
 || act.rem_1?
}

predicate Internal(act: Action) {
    false
}

lemma Action_Set_Disjoint(act: Action)
    ensures !(Output(act) && Input(act))
         && !(Output(act) && Internal(act))
         && !(Internal(act) && Input(act))
{}

predicate External(act: Action) {
    Input(act) || Output(act)
}

predicate Signature(act: Action) {
    External(act) || Internal(act)
}

predicate Initial(s: State) {
    s.reg_0.rem?
 && s.reg_1.rem?
}

predicate pre_try_0(act: Action, s: State)
{   Input(act) && act.try_0?}

function  eff_try_0(act: Action, s: State): State
    requires pre_try_0(act, s)
{
    var s: State := s.(reg_0 := try); s
}

predicate pre_crit_0(act: Action, s: State) {
    Output(act) && act.crit_0?
 && (s.reg_0 == try && s.reg_1 != crit)
}

function  eff_crit_0(act: Action, s: State): State
    requires pre_crit_0(act, s)
{
    var s: State := s.(reg_0 := crit); s
}

predicate pre_exit_0(act: Action, s: State)
{   Input(act) && act.exit_0?}

function  eff_exit_0(act: Action, s: State): State
    requires pre_exit_0(act, s)
{
    var s: State := s.(reg_0 := exit); s
}

predicate pre_rem_0(act: Action, s: State) {
    Output(act) && act.rem_0?
 && (s.reg_0 == exit)
}

function  eff_rem_0(act: Action, s: State): State
    requires pre_rem_0(act, s)
{
    var s: State := s.(reg_0 := rem); s
}

predicate pre_try_1(act: Action, s: State)
{   Input(act) && act.try_1?}

function  eff_try_1(act: Action, s: State): State
    requires pre_try_1(act, s)
{
    var s: State := s.(reg_1 := try); s
}

predicate pre_crit_1(act: Action, s: State) {
    Output(act) && act.crit_1?
 && (s.reg_1 == try && s.reg_0 != crit)
}

function  eff_crit_1(act: Action, s: State): State
    requires pre_crit_1(act, s)
{
    var s: State := s.(reg_1 := crit); s
}

predicate pre_exit_1(act: Action, s: State)
{   Input(act) && act.exit_1?}

function  eff_exit_1(act: Action, s: State): State
    requires pre_exit_1(act, s)
{
    var s: State := s.(reg_1 := exit); s
}

predicate pre_rem_1(act: Action, s: State) {
    Output(act) && act.rem_1?
 && (s.reg_1 == exit)
}

function  eff_rem_1(act: Action, s: State): State
    requires pre_rem_1(act, s)
{
    var s: State := s.(reg_1 := rem); s
}

predicate Transition(s: State, act: Action, s': State) {
    (pre_try_0(act, s) && s' == eff_try_0(act, s))
 || (pre_crit_0(act, s) && s' == eff_crit_0(act, s))
 || (pre_exit_0(act, s) && s' == eff_exit_0(act, s))
 || (pre_rem_0(act, s) && s' == eff_rem_0(act, s))
 || (pre_try_1(act, s) && s' == eff_try_1(act, s))
 || (pre_crit_1(act, s) && s' == eff_crit_1(act, s))
 || (pre_exit_1(act, s) && s' == eff_exit_1(act, s))
 || (pre_rem_1(act, s) && s' == eff_rem_1(act, s))
}

lemma Input_Enabled(act: Action, s: State)
    requires Input(act)
    ensures  exists s': State :: Transition(s, act, s')
{
    var s': State :| Transition(s, act, s');
}
}
