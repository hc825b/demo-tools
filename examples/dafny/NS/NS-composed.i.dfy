include "../Framework/Types.s.dfy"
include "../Framework/Environment.s.dfy"
include "../Framework/Attacker.s.dfy"

include "NS.i.dfy"

module NS_Composed {

import opened Types

import Env = Environment
import Att = Attacker
import P = NS
datatype SysParameter = SysParameter(i1 : P.Parameter, r1 : P.Parameter, i2 : P.Parameter, r2 : P.Parameter)
datatype State = State( att : Att.State, env : Env.State
                      , i1 : P.State, r1 : P.State
                      , i2 : P.State, r2 : P.State)

predicate Compatible(para : SysParameter) {
    forall act : Action ::
        (P.OutputAction(para.i1, act) && P.OutputAction(para.r1, act) <==> false)
     && (P.OutputAction(para.i1, act) && P.OutputAction(para.i2, act) <==> false)
     && (P.OutputAction(para.i1, act) && P.OutputAction(para.r2, act) <==> false)
     && (P.OutputAction(para.r1, act) && P.OutputAction(para.i2, act) <==> false)
     && (P.OutputAction(para.r1, act) && P.OutputAction(para.r2, act) <==> false)
     && (P.OutputAction(para.i2, act) && P.OutputAction(para.r2, act) <==> false)
}

predicate Initial(s : State) {
    Env.Initial(s.env)
 && Att.Initial(s.att)
 && P.Initial(s.i1) && P.Initial(s.r1)
 && P.Initial(s.i2) && P.Initial(s.r2)
}

predicate ValidAction(para : SysParameter, act : Action) {
    InputAction(para, act) || OutputAction(para, act)
}

predicate InputAction(para : SysParameter, act : Action) {
    ( Env.InputAction(act)
   || Att.InputAction(act)
   || P.InputAction(para.i1, act)
   || P.InputAction(para.r1, act)
   || P.InputAction(para.i2, act)
   || P.InputAction(para.r2, act)
    ) && ! OutputAction(para, act)
}

predicate OutputAction(para : SysParameter, act : Action) {
    Env.OutputAction(act)
 || Att.OutputAction(act)
 || P.OutputAction(para.i1, act)
 || P.OutputAction(para.r1, act)
 || P.OutputAction(para.i2, act)
 || P.OutputAction(para.r2, act)
}

predicate Transition(para : SysParameter, s: State, act: Action, s': State) {
    // TODO figure out when does act belongs to the Action set of a component
//    (if Env.ValidAction(act) then Env.Transition(s.env, act, s'.env) else s'.env == s.env)
    (if Env.ValidAction(act) then
        (s.env.q == [] || act.recv?)
     && Env.Transition(s.env, act, s'.env)
     else s'.env == s.env)
 && (if Att.ValidAction(act) then Att.Transition(s.att, act, s'.att) else s'.att == s.att)
 && (if P.ValidAction(para.i1, act) then P.Transition(para.i1, s.i1, act, s'.i1) else s'.i1 == s.i1)
 && (if P.ValidAction(para.r1, act) then P.Transition(para.r1, s.r1, act, s'.r1) else s'.r1 == s.r1)
 && (if P.ValidAction(para.i2, act) then P.Transition(para.i2, s.i2, act, s'.i2) else s'.i2 == s.i2)
 && (if P.ValidAction(para.r2, act) then P.Transition(para.r2, s.r2, act, s'.r2) else s'.r2 == s.r2)
}

lemma underapproxEnvironment(s : State, act : Action, s' : State)
    ensures
    (if Env.ValidAction(act) then
        (s.env.q == [] || act.recv?) && Env.Transition(s.env, act, s'.env)
     else s'.env == s.env)
 ==>(if Env.ValidAction(act) then
        Env.Transition(s.env, act, s'.env)
     else s'.env == s.env)
{}

}

module NS_Proof {

import opened Types
import opened NS_Composed
import Att = Attacker

predicate Prop_Secrect_nr(para : SysParameter, s : State) {
    // TODO specify the property
    !Entail(s.att.know, Fresh(para.r2.n))
 //&& forall msg :: msg in s.env.q ==> msg.2 != Fresh(para.r2.n)
}

const ni1 : int := 10
const nr1 : int := 20
const ni2 : int := 30
const nr2 : int := 40

function ValidSystemParameter() : SysParameter
    ensures Compatible(ValidSystemParameter())
{
    var para_i1 : P.Parameter := P.Parameter(1, RoleI, A, E, ni1);
    var para_r1 : P.Parameter := P.Parameter(2, RoleR, A, E, nr1);
    var para_i2 : P.Parameter := P.Parameter(3, RoleI, A, B, ni2);
    var para_r2 : P.Parameter := P.Parameter(4, RoleR, A, B, nr2);
    SysParameter(para_i1, para_r1, para_i2, para_r2)
}

lemma test_Entail(s : State)
    ensures Initial(s) ==> !Entail(s.att.know, Fresh(nr2))
{}

lemma textbook_Prop_Secret_nr_Init()
    ensures forall s : State ::
        Initial(s) ==> Prop_Secrect_nr(ValidSystemParameter(), s)
{}

/*
lemma textbook_Prop_Secret_nr_Inductive()
    ensures forall s, act, s' ::
        var para := ValidSystemParameter();
        (Prop_Secrect_nr(para, s) &&
        OutputAction(para, act) && Transition(para, s, act, s'))
        ==> Prop_Secrect_nr(para, s')
{}
*/

lemma textbook_Prop_Secret_nr_BMC(
    s0:State
  , a1:Action, s1:State
  , a2:Action, s2:State
  , a3:Action, s3:State
  , a4:Action, s4:State
  , a5:Action, s5:State
  , a6:Action, s6:State
  , a7:Action, s7:State
  , a8:Action, s8:State
  , a9:Action, s9:State
  , a10:Action, s10:State
)
{
    var sys_para : SysParameter := ValidSystemParameter();

    assume Initial(s0);
    assert Prop_Secrect_nr(sys_para, s0); // Assertion should pass.

    assume OutputAction(sys_para, a1) && Transition(sys_para, s0, a1, s1);
//    assume a1 == send(1, A, E, Enc(Pair(Fresh(ni1), AgentTerm(A)), PK(E)));
    assert Prop_Secrect_nr(sys_para, s1); // Assertion should pass.

    assume OutputAction(sys_para, a2) && Transition(sys_para, s1, a2, s2);
//    assert a2 == recv(A, E, Enc(Pair(Fresh(ni1), AgentTerm(A)), PK(E)));
    assert Prop_Secrect_nr(sys_para, s2); // Assertion should pass.

    assume OutputAction(sys_para, a3) && Transition(sys_para, s2, a3, s3);
//    assume a3 == send(0, A, B, Enc(Pair(Fresh(ni1), AgentTerm(A)), PK(B)));
    assert Prop_Secrect_nr(sys_para, s3); // Assertion should pass.

    assume OutputAction(sys_para, a4) && Transition(sys_para, s3, a4, s4);
//    assert a4 == recv(A, B, Enc(Pair(Fresh(ni1), AgentTerm(A)), PK(B)));
    assert Prop_Secrect_nr(sys_para, s4); // Assertion should pass.

    assume OutputAction(sys_para, a5) && Transition(sys_para, s4, a5, s5);
//    assume a5 == send(4, B, A, Enc(Pair(Fresh(ni1), Fresh(nr2)), PK(A)));
    assert Prop_Secrect_nr(sys_para, s5); // Assertion should pass.

    assume OutputAction(sys_para, a6) && Transition(sys_para, s5, a6, s6);
//    assert a6 == recv(B, A, Enc(Pair(Fresh(ni1), Fresh(nr2)), PK(A)));
    assert Prop_Secrect_nr(sys_para, s6); // Assertion should pass.

    assume OutputAction(sys_para, a7) && Transition(sys_para, s6, a7, s7);
//    assume a7 == send(0, E, A, Enc(Pair(Fresh(ni1), Fresh(nr2)), PK(A)));
    assert Prop_Secrect_nr(sys_para, s7); // Assertion should pass.

    assume OutputAction(sys_para, a8) && Transition(sys_para, s7, a8, s8);
//    assert a8 == recv(E, A, Enc(Pair(Fresh(ni1), Fresh(nr2)), PK(A)));
    assert Prop_Secrect_nr(sys_para, s8); // Assertion should pass.

    assume OutputAction(sys_para, a9) && Transition(sys_para, s8, a9, s9);
//    assume a9 == send(1, A, E, Enc(Fresh(nr2), PK(E)));
    assert Prop_Secrect_nr(sys_para, s9); // Assertion should pass.

    assume OutputAction(sys_para, a10) && Transition(sys_para, s9, a10, s10);
//    assert a10 == recv(A, E, Enc(Fresh(nr2), PK(E)));
    assert Prop_Secrect_nr(sys_para, s10); // Assertion should fail.
}

}
