include "../Framework/Types.s.dfy"

module NS {

import opened Types

newtype Loc = pc : nat | 0 <= pc < 5

datatype Parameter = Parameter( rid: RunId, role : Role
                              , i : Agent, r : Agent
                              , n : Nonce)

datatype State = State(pc : Loc, V : Nonce, W : Nonce)

predicate Initial(s : State) {
    s.pc == 1
}

predicate ValidAction(para : Parameter, act : Action) {
    InputAction(para, act) || OutputAction(para, act)
}

predicate InputAction(para : Parameter, act : Action) {
    act.recv?
}

predicate OutputAction(para : Parameter, act : Action) {
    act.send? && act.rid == para.rid
}

predicate pre_send(para : Parameter, act: Action, s: State)
{
    act.send? && var send(rid, aS, aR, term) := act; para.rid == rid
 && match para.role {
        case RoleI =>
           (aS == para.i && aR == para.r && s.pc == 1
            && term == Enc(Pair(Fresh(para.n), AgentTerm(para.i)), PK(para.r)))
         ||(aS == para.i && aR == para.r && s.pc == 3
            && term == Enc(Fresh(s.V), PK(para.r)))
        case RoleR =>
            aS == para.r && aR == para.i && s.pc == 2
         && term == Enc(Pair(Fresh(s.W), Fresh(para.n)), PK(para.i))
    }
}

function  eff_send(para : Parameter, s : State, act : Action) : State
    requires pre_send(para, act, s)
{
    s.(pc := s.pc + 1)
}

predicate pre_recv(para : Parameter, act: Action, s : State) {
    act.recv?
}
/*
function  eff_recv(para : Parameter, s : State, act : Action) : State
    requires pre_recv(para, act, s)
{
    // TODO How to implement Match
    var recv(aS, aR, term) := act;
    match para.role {
        case RoleI =>
            if aS == para.r && aR == para.i && s.pc == 2
            && Match(Enc(Pair(Fresh(para.n), Fresh(s.V)), PK(para.i)), term) then
                s.(pc := s.pc + 1)
            else
                s
        case RoleR =>
            if aS == para.i && aR == para.r
            && ((s.pc == 1
                && Match(Enc(Pair(Fresh(s.W), AgentTerm(para.i)), PK(para.r)), term))
             || (s.pc == 3
                && Match(Enc(Fresh(para.n), PK(para.r)), term)))
            then
                s.(pc := s.pc + 1)
            else
                s
    }
}*/

predicate rel_eff_recv(para : Parameter, s : State, act : Action, s' : State)
    requires pre_recv(para, act, s)
{
    var recv(aS, aR, term) := act;
    match para.role {
        case RoleI =>
            if aS == para.r && aR == para.i then
                exists x : Nonce ::
                if s.pc == 2
                && Enc(Pair(Fresh(para.n), Fresh(x)), PK(para.i)) == term then
                    s' == s.(pc := s.pc + 1, V := x)
                else
                    s' == s.(pc := 0) // Abort
            else
                s' == s
        case RoleR =>
            if aS == para.i && aR == para.r then
                exists x : Nonce ::
                if s.pc == 1
                && Enc(Pair(Fresh(x), AgentTerm(para.i)), PK(para.r)) == term then
                    s' == s.(pc := s.pc + 1, W := x)
                else
                if s.pc == 3 && Enc(Fresh(para.n), PK(para.r)) == term then
                    s' == s.(pc := s.pc + 1)
                else
                    s' == s.(pc := 0) // Abort
            else
                s' == s
    }
}

predicate Transition(para : Parameter, s : State, act : Action, s' : State) {
    (pre_send(para, act, s) && eff_send(para, s, act) == s')
 || (pre_recv(para, act, s) && rel_eff_recv(para, s, act, s'))
}

}
