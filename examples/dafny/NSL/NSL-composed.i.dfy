include "../Framework/Types.s.dfy"
include "../Framework/Environment.s.dfy"
include "../Framework/Attacker.s.dfy"

include "NSL.i.dfy"

module NS_Composed {

import opened Types

import Env = Environment
import Att = Attacker
import P = NS
datatype SysParameter = SysParameter(p1 : P.Parameter, p2 : P.Parameter, p3 : P.Parameter, p4 : P.Parameter)
datatype State = State( att : Att.State, env : Env.State
                      , p1 : P.State, p2 : P.State
                      , p3 : P.State, p4 : P.State)

predicate Compatible(para : SysParameter) {
    forall act : Action ::
        (P.OutputAction(para.p1, act) && P.OutputAction(para.p2, act) <==> false)
     && (P.OutputAction(para.p1, act) && P.OutputAction(para.p3, act) <==> false)
     && (P.OutputAction(para.p1, act) && P.OutputAction(para.p4, act) <==> false)
     && (P.OutputAction(para.p2, act) && P.OutputAction(para.p3, act) <==> false)
     && (P.OutputAction(para.p2, act) && P.OutputAction(para.p4, act) <==> false)
     && (P.OutputAction(para.p3, act) && P.OutputAction(para.p4, act) <==> false)
}

predicate Initial(s : State) {
    Env.Initial(s.env)
 && Att.Initial(s.att)
 && P.Initial(s.p1) && P.Initial(s.p2)
 && P.Initial(s.p3) && P.Initial(s.p4)
}

predicate ValidAction(para : SysParameter, act : Action) {
    InputAction(para, act) || OutputAction(para, act)
}

predicate InputAction(para : SysParameter, act : Action) {
    ( Env.InputAction(act)
   || Att.InputAction(act)
   || P.InputAction(para.p1, act)
   || P.InputAction(para.p2, act)
   || P.InputAction(para.p3, act)
   || P.InputAction(para.p4, act)
    ) && ! OutputAction(para, act)
}

predicate OutputAction(para : SysParameter, act : Action) {
    Env.OutputAction(act)
 || Att.OutputAction(act)
 || P.OutputAction(para.p1, act)
 || P.OutputAction(para.p2, act)
 || P.OutputAction(para.p3, act)
 || P.OutputAction(para.p4, act)
}

predicate Transition(para : SysParameter, s: State, act: Action, s': State) {
    // TODO figure out when does act belongs to the Action set of a component
//    (if Env.ValidAction(act) then Env.Transition(s.env, act, s'.env) else s'.env == s.env)
    (if Env.ValidAction(act) then
        (s.env.q == [] || act.recv?)
     && Env.Transition(s.env, act, s'.env)
     else s'.env == s.env)
 && (if Att.ValidAction(act) then Att.Transition(s.att, act, s'.att) else s'.att == s.att)
 && (if P.ValidAction(para.p1, act) then P.Transition(para.p1, s.p1, act, s'.p1) else s'.p1 == s.p1)
 && (if P.ValidAction(para.p2, act) then P.Transition(para.p2, s.p2, act, s'.p2) else s'.p2 == s.p2)
 && (if P.ValidAction(para.p3, act) then P.Transition(para.p3, s.p3, act, s'.p3) else s'.p3 == s.p3)
 && (if P.ValidAction(para.p4, act) then P.Transition(para.p4, s.p4, act, s'.p4) else s'.p4 == s.p4)
}

lemma underapproxEnvironment(s : State, act : Action, s' : State)
    ensures
    (if Env.ValidAction(act) then
        (s.env.q == [] || act.recv?) && Env.Transition(s.env, act, s'.env)
     else s'.env == s.env)
 ==>(if Env.ValidAction(act) then
        Env.Transition(s.env, act, s'.env)
     else s'.env == s.env)
{}

}

module NS_Proof {

import opened Types
import opened NS_Composed
import Att = Attacker

predicate Prop_Secrect_nr(para : SysParameter, s : State) {
    // TODO specify the property
    !Entail(s.att.know, Fresh(para.p2.n))
 //&& forall msg :: msg in s.env.q ==> msg.2 != Fresh(para.p4.n)
}

const n1 : int := 10
const n2 : int := 20
const n3 : int := 30
const n4 : int := 40

function ValidSystemParameter() : SysParameter
    ensures Compatible(ValidSystemParameter())
{
    var para_p1 : P.Parameter := P.Parameter(1, RoleI, A, B, n1);
    var para_p2 : P.Parameter := P.Parameter(2, RoleR, A, B, n2);
    var para_p3 : P.Parameter := P.Parameter(3, RoleI, A, E, n3);
    var para_p4 : P.Parameter := P.Parameter(4, RoleR, A, E, n4);
    SysParameter(para_p1, para_p2, para_p3, para_p4)
}

lemma test_Entail(s : State)
    ensures Initial(s) ==> !Entail(s.att.know, Fresh(n2))
{}

lemma textbook_Prop_Secret_nr_Init()
    ensures forall s : State ::
        Initial(s) ==> Prop_Secrect_nr(ValidSystemParameter(), s)
{}

/*
lemma textbook_Prop_Secret_nr_Inductive()
    ensures forall s, act, s' ::
        var para := ValidSystemParameter();
        (Prop_Secrect_nr(para, s) &&
        OutputAction(para, act) && Transition(para, s, act, s'))
        ==> Prop_Secrect_nr(para, s')
{}
*/

lemma textbook_Prop_Secret_nr_BMC(
    s0:State
  , a1:Action, s1:State
  , a2:Action, s2:State
  , a3:Action, s3:State
  , a4:Action, s4:State
  , a5:Action, s5:State
  , a6:Action, s6:State
  , a7:Action, s7:State
  , a8:Action, s8:State
  , a9:Action, s9:State
  , a10:Action, s10:State
)
{
    var sys_para : SysParameter := ValidSystemParameter();

    assume Initial(s0);
    assert Prop_Secrect_nr(sys_para, s0); // Assertion should pass.

    assume OutputAction(sys_para, a1) && Transition(sys_para, s0, a1, s1);
    assert Prop_Secrect_nr(sys_para, s1); // Assertion should pass.

    assume OutputAction(sys_para, a2) && Transition(sys_para, s1, a2, s2);
    assert Prop_Secrect_nr(sys_para, s2); // Assertion should pass.

    assume OutputAction(sys_para, a3) && Transition(sys_para, s2, a3, s3);
    assert Prop_Secrect_nr(sys_para, s3); // Assertion should pass.

    assume OutputAction(sys_para, a4) && Transition(sys_para, s3, a4, s4);
    assert Prop_Secrect_nr(sys_para, s4); // Assertion should pass.

    assume OutputAction(sys_para, a5) && Transition(sys_para, s4, a5, s5);
    assert Prop_Secrect_nr(sys_para, s5); // Assertion should pass.

    assume OutputAction(sys_para, a6) && Transition(sys_para, s5, a6, s6);
    assert Prop_Secrect_nr(sys_para, s6); // Assertion should pass.

    assume OutputAction(sys_para, a7) && Transition(sys_para, s6, a7, s7);
    assert Prop_Secrect_nr(sys_para, s7); // Assertion should pass.

    assume OutputAction(sys_para, a8) && Transition(sys_para, s7, a8, s8);
    assert Prop_Secrect_nr(sys_para, s8); // Assertion should pass.

    assume OutputAction(sys_para, a9) && Transition(sys_para, s8, a9, s9);
    assert Prop_Secrect_nr(sys_para, s9); // Assertion should pass.

    assume OutputAction(sys_para, a10) && Transition(sys_para, s9, a10, s10);
    assert Prop_Secrect_nr(sys_para, s10); // Assertion should fail.
}

}
