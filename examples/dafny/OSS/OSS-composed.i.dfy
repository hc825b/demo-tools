include "OSS-types.i.dfy"

module OSS_Composed {

import opened OSS_Type

newtype Loc = pc : nat | 0 <= pc < 2

type Message = (Agent, Agent, RunTerm)

datatype RoleStateT = RoleStateT(pc: Loc, inst: InstT)

datatype StateT = StateT(i1: RoleStateT, r1: RoleStateT, know: Knowledge, q:seq<Message>)

predicate Initial(s : StateT) {
    s.r1.pc == 0 && s.r1.inst == map[]
 && s.i1.pc == 0 && s.i1.inst == map[]
 && s.know == {AgentTerm(A), AgentTerm(B), PK(AgentTerm(B)), Fresh("ne")}
 && s.q == []
}

predicate pre_send_I1(act: ActionT, s: StateT)
{
    act.send? && var send(pid, aS, aR, term) := act;
    pid == 0 && aS == A && aR == B && term == Enc(Pair(AgentTerm(A), Fresh("ni")), PK(AgentTerm(B)))
}

function  eff_send_I1(s: StateT, act: ActionT) : StateT
    requires pre_send_I1(act, s)
{
    var send(pid, aS, aR, term) := act;
    s.(q := s.q + [(aS, aR, term)], i1 := s.i1.(pc := 1))
}

predicate pre_send_R1(act: ActionT, s: StateT)
{
    act.send? && var send(pid, aS, aR, term) := act;
    pid == 1 && false
}

function  eff_send_R1(s: StateT, act: ActionT) : StateT
    requires pre_send_R1(act, s)
{
    var send(pid, aS, aR, term) := act;
    s.(q := s.q + [(aS, aR, term)])
}

predicate pre_send_A(act: ActionT, s: StateT)
{
    act.send? && var send(pid, aS, aR, term) := act;
    pid == 2 && Entail(s.know, term)
}

function  eff_send_A(s: StateT, act: ActionT) : StateT
    requires pre_send_A(act, s) 
{
    var send(pid, aS, aR, term) := act;
    s.(q := s.q + [(aS, aR, term)])
}

predicate pre_recv(act: ActionT, s: StateT)
{
    act.recv? && var recv(aS, aR, term) := act;
    s.q != [] && s.q[0] == (aS, aR, term)
}

predicate eff_recv(s:StateT, act: ActionT, s' : StateT)
    requires pre_recv(act, s)
{
    var recv(aS, aR, term) := act;
    s'.q == s.q[1..]
 && s'.know == s.know + {term}
 && s'.i1 == s.i1
 &&((s.r1.pc == 0 && aS == A && aR == B
    && Match(s.r1.inst, term, Enc(Pair(AgentTerm(A), Var("W")), PK(AgentTerm(B))), s'.r1.inst)
    && s'.r1.pc == 1)
    || s'.r1 == s.r1 // XXX can it always ignore messages?
   )
}

predicate Transition(s:StateT, act: ActionT, s':StateT) {
    (pre_send_I1(act, s) && eff_send_I1(s, act) == s')
 || (pre_send_R1(act, s) && eff_send_R1(s, act) == s')
 || (pre_send_A(act, s)  && eff_send_A(s, act) == s')
 || (pre_recv(act, s)    && eff_recv(s, act, s'))
}

}

module OSS_Proof {

import opened OSS_Type
import opened OSS_Composed

predicate Invariant_Secret_ni(s: StateT) {
    !Entail(s.know, Fresh("ni"))
 && (forall msg :: msg in s.q ==> msg.2 != Fresh("ni"))
}

lemma textbook_Invariant_Secret_ni_Init()
    ensures forall s : StateT :: Initial(s) ==> Invariant_Secret_ni(s)
{ /* This should pass */ }

lemma textbook_Invariant_Secret_ni_Inductive()
    ensures forall s, act, s' ::
        Invariant_Secret_ni(s) && Transition(s, act, s') ==> Invariant_Secret_ni(s')
{ /* This should pass */ }

predicate NotAnInvariant(s : StateT) {
    !Entail(s.know, Enc(Pair(AgentTerm(A), Fresh("ni")), PK(AgentTerm(B))))
}

lemma textbook_NotAnInvariant_Init()
    ensures forall s : StateT :: Initial(s) ==> NotAnInvariant(s)
{ /* This should pass */ }

lemma textbook_NotAnInvariant_BMC(
    s0:StateT
  , a1:ActionT, s1:StateT
  , a2:ActionT, s2:StateT
)
{
    assume Initial(s0);
    assert NotAnInvariant(s0); // Assertion should pass.

    assume Transition(s0, a1, s1);
    assert NotAnInvariant(s1); // Assertion should pass.

    assume Transition(s1, a2, s2);
    assert NotAnInvariant(s2); // Assertion should fail.
}

predicate Property_Secret_W(s: StateT) {
    !Entail(s.know, Instantiate(s.r1.inst, Var("W")))
}

lemma textbook_Property_Secret_W_Init()
    ensures forall s: StateT :: Initial(s) ==> Property_Secret_W(s)
{}

lemma test() {
    assert Match( map[]
                , Enc(Pair(AgentTerm(A), Fresh("ni")), PK(AgentTerm(B)))
                , Enc(Pair(AgentTerm(A), Var("W")), PK(AgentTerm(B)))
                , map["W" := Fresh("ni")]
                );
}

lemma textbook_Property_Secret_W_BMC(
    s0:StateT
  , a1:ActionT, s1:StateT
  , a2:ActionT, s2:StateT
)
{
    assume Initial(s0);
    assert Property_Secret_W(s0); // Assertion should pass.

    assume Transition(s0, a1, s1);
    assert Property_Secret_W(s1); // Assertion should pass.

    assume Transition(s1, a2, s2);
    assert Property_Secret_W(s2); // Assertion should fail.
}

}
