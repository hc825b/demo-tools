module OSS_Type {

type Id = nat

datatype Role = RoleI | RoleR
datatype Agent = A | B | E

type VarId = string

datatype RunTerm = Fresh(string) | Var(id:VarId)
                 | AgentTerm(Agent) | Pair(RunTerm, RunTerm)
                 | PK(RunTerm) | SK(RunTerm)
                 | Enc(msg:RunTerm, key:RunTerm)

// datatype ClaimType = SECRET

datatype ActionT = send(Id, Agent, Agent, term : RunTerm)
                 | recv(Agent, Agent, term : RunTerm)
                 // | claim(Id, Agent, ClaimType, RunTerm)

type InstT = map<VarId, RunTerm>

type Knowledge = set<RunTerm>

function Instantiate(inst : InstT, rt : RunTerm) : RunTerm {
    // TODO implement instantiation
    if rt.Var? && rt.id in inst then
        inst[rt.id]
    else
        rt
}

predicate Match(inst: InstT, value: RunTerm, pattern: RunTerm, inst': InstT) {
    // TODO implement simple unification algorithm
    pattern == Enc(Pair(AgentTerm(A), Var("W")), PK(AgentTerm(B)))
 &&( ( value == Enc(Pair(AgentTerm(A), Fresh("ni")), PK(AgentTerm(B)))
       && inst' == inst["W" := Fresh("ni")] )
   ||( value == Enc(Pair(AgentTerm(A), Fresh("ne")), PK(AgentTerm(B)))
       && inst' == inst["W" := Fresh("ne")])
   )
}

predicate Entail(know:Knowledge, term: RunTerm) {
    // TODO implement simple inference
       term in know
    ||({Fresh("ne"), AgentTerm(A), PK(AgentTerm(B))} <= know
       && term == Enc(Pair(AgentTerm(A), Fresh("ne")), PK(AgentTerm(B))))
}

} // End module


