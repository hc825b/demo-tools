include "Peterson.i.dfy"
include "Peterson-composed.i.dfy"

module Peterson2_InvProof {
import opened Types
import opened Peterson2

predicate Invariant(s: State) {
   !( (s.p0.pc == critical0 || s.p0.pc == critical1) &&
      (s.p1.pc == critical0 || s.p1.pc == critical1)
    )
 &&
    AuxInvariant(s)
}

predicate AuxInvariant(s: State) {
    // Variables are synced
    ((s.p0.pc == trying2 || s.p1.pc == trying2)
    ==> s.p0.turn == s.p1.turn)
 && s.p0.flag(p0) == s.p1.flag(p0)
 && s.p0.flag(p1) == s.p1.flag(p1)

 && ((s.p0.pc == trying1
   || s.p0.pc == trying2
   || s.p0.pc == critical0
   || s.p0.pc == critical1
    ) ==> s.p0.flag(p0))
 && ((s.p1.pc == trying1
   || s.p1.pc == trying2
   || s.p1.pc == critical0
   || s.p1.pc == critical1
    ) ==> s.p1.flag(p1))

 && (s.p0.pc == trying2 && s.p0.turn == p1 ==> s.p1.pc == trying2)
 && (s.p1.pc == trying2 && s.p1.turn == p0 ==> s.p0.pc == trying2)
}

lemma BMC_Invariant(
                  s0: State
    , a1: Action, s1: State
)
    requires Initial(s0)
    ensures  Invariant(s0)
    requires Transition(s0, a1, s1)
    ensures  Invariant(s1)
{}

lemma Inductive_Invariant(
                  s0: State
    , a1: Action, s1: State
    , a2: Action, s2: State
)
    requires Invariant(s0) && Transition(s0, a1, s1)
    requires Invariant(s1) && Transition(s1, a2, s2)
    ensures  Invariant(s2)
{}

}
