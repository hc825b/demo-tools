include "Peterson.i.dfy"

module Peterson2 {
import opened Types
import P = Peterson 

datatype State = State(p0: P.State, p1: P.State)

const para_p0 := P.Parameter(p0)
const para_p1 := P.Parameter(p1)

predicate Initial(s: State) {
    P.Initial(s.p0, para_p0)
 && P.Initial(s.p1, para_p1)
}

predicate Output(act: Action) {
    P.Output(act, para_p0)
 || P.Output(act, para_p1)
}

predicate Input(act: Action) {
   (P.Input(act, para_p0) || P.Input(act, para_p1))
 && !Output(act)
}

predicate Internal(act: Action) {
    P.Internal(act, para_p0)
 || P.Internal(act, para_p1)
}

lemma Action_Set_Disjoint(act: Action)
    ensures !(Output(act) && Input(act))
         && !(Output(act) && Internal(act))
         && !(Internal(act) && Input(act))
{}

predicate External(act: Action) {
    Input(act) || Output(act)
}

predicate Signature(act: Action) {
    External(act) || Internal(act)
}

lemma Compatibility(act: Action)
    ensures !(P.Output(act, para_p0) && P.Output(act, para_p1))
         && !(P.Internal(act, para_p0) && P.Signature(act, para_p1))
         && !(P.Signature(act, para_p0) && P.Internal(act, para_p1))
{}

predicate Transition(s: State, act: Action, s': State) {
    (if P.Signature(act, para_p0)
     then P.Transition(s.p0, act, s'.p0, para_p0)
     else s'.p0 == s.p0)
 && (if P.Signature(act, para_p1)
     then P.Transition(s.p1, act, s'.p1, para_p1)
     else s'.p1 == s.p1)
}


}
