module Types {

datatype ProcType = p0 | p1

datatype PCType = waiting
                | trying0 | trying1 | trying2
                | critical0 | critical1

datatype Action = try(p: ProcType)
                | critical(p: ProcType)
                | release(p: ProcType)
                | setFlag(p: ProcType)
                | setTurn(p: ProcType)
                | checkFlag(p: ProcType)
                | checkTurn(p: ProcType)
}

module Peterson {

import opened Types

datatype State = State(pc: PCType
                      ,flag: (ProcType -> bool)
                      ,turn: ProcType
                      )

datatype Parameter = Parameter(p: ProcType)

predicate Output(act: Action, para: Parameter) {
    (act.try? && act.p == para.p)
 || (act.critical? && act.p == para.p)
 || (act.release? && act.p == para.p)
 || (act.setFlag? && act.p == para.p)
 || (act.setTurn? && act.p == para.p)
  
}

predicate Input(act: Action, para: Parameter) {
    (act.setFlag? && act.p != para.p)
 || (act.setTurn? && act.p != para.p)
 || (act.release? && act.p != para.p)
}

predicate Internal(act: Action, para: Parameter) {
    (act.checkFlag? && act.p == para.p)
 || (act.checkTurn? && act.p == para.p)
}

lemma Action_Set_Disjoint(act: Action, para: Parameter)
    ensures !(Output(act, para) && Input(act, para))
         && !(Output(act, para) && Internal(act, para))
         && !(Internal(act, para) && Input(act, para))
{}

predicate External(act: Action, para: Parameter) {
    Input(act, para) || Output(act, para)
}

predicate Signature(act: Action, para: Parameter) {
    External(act, para) || Internal(act, para)
}

predicate Initial(s: State, para: Parameter) {
    s.pc == waiting
 && s.flag == ((x: ProcType) => false) 
}

predicate pre_try(act: Action, s: State, para: Parameter) {
    Output(act, para) && act.try? && s.pc == waiting
}

function  eff_try(act: Action, s: State, para: Parameter): State
    requires pre_try(act, s, para)
{
    var s := s.(pc := trying0); s
}

predicate pre_setFlag_1(act: Action, s: State, para: Parameter) {
    Output(act, para) && act.setFlag? && s.pc == trying0
}

function  eff_setFlag_1(act: Action, s: State, para: Parameter): State
    requires pre_setFlag_1(act, s, para)
{
    var s := s.(pc := trying1);
    var s := s.(flag := ((x: ProcType) => if x == act.p then true else s.flag(x))); s
}

predicate pre_setFlag_2(act: Action, s: State, para: Parameter) {
    Input(act, para) && act.setFlag?
}

function  eff_setFlag_2(act: Action, s: State, para: Parameter): State
    requires pre_setFlag_2(act, s, para)
{
    var s := s.(flag := ((x: ProcType) => if x == act.p then true else s.flag(x))); s
}

predicate pre_setTurn_1(act: Action, s: State, para: Parameter) {
    Output(act, para) && act.setTurn? && s.pc == trying1
}

function  eff_setTurn_1(act: Action, s: State, para: Parameter): State
    requires pre_setTurn_1(act, s, para)
{
    var s := s.(pc := trying2);
    var s := s.(turn := act.p); s
}

predicate pre_setTurn_2(act: Action, s: State, para: Parameter) {
    Input(act, para) && act.setTurn?
}

function  eff_setTurn_2(act: Action, s: State, para: Parameter): State
    requires pre_setTurn_2(act, s, para)
{
    var s := s.(turn := act.p); s
}

predicate pre_checkTurn(act: Action, s: State, para: Parameter) {
    Internal(act, para) && act.checkTurn? && s.pc == trying2
}

function  eff_checkTurn(act: Action, s: State, para: Parameter): State
    requires pre_checkTurn(act, s, para)
{
    var s := (if s.turn != act.p
              then var s := s.(pc := critical0); s
              else s); s
}

predicate pre_checkFlag(act: Action, s: State, para: Parameter) {
    Internal(act, para) && act.checkFlag? && s.pc == trying2
}

function  eff_checkFlag(act: Action, s: State, para: Parameter): State
    requires pre_checkFlag(act, s, para)
{
    // Element not in domain because s.flag is not initialized?
    var s := (if !s.flag(if act.p == p0 then p1 else p0)
              then var s := s.(pc := critical0); s
              else s); s
}

predicate pre_critical(act: Action, s: State, para: Parameter) {
    Output(act, para) && act.critical? && s.pc == critical0
}

function  eff_critical(act: Action, s: State, para: Parameter): State
    requires pre_critical(act, s, para)
{
    var s := s.(pc := critical1); s
}

predicate pre_release_1(act: Action, s: State, para: Parameter) {
    Output(act, para) && act.release? && s.pc == critical1
}

function  eff_release_1(act: Action, s: State, para: Parameter): State
    requires pre_release_1(act, s, para)
{
    var s := s.(pc := waiting);
    var s := s.(flag := ((x: ProcType) => if x == act.p then false else s.flag(x))); s
}

predicate pre_release_2(act: Action, s: State, para: Parameter) {
    Input(act, para) && act.release?
}

function  eff_release_2(act: Action, s: State, para: Parameter): State
    requires pre_release_2(act, s, para)
{
    var s := s.(flag := ((x: ProcType) => if x == act.p then false else s.flag(x))); s
}

predicate Transition(s: State, act: Action, s': State, para: Parameter)
{
    (pre_try(act, s, para) && s' == eff_try(act, s, para))
 || (pre_setFlag_1(act, s, para) && s' == eff_setFlag_1(act, s, para))
 || (pre_setFlag_2(act, s, para) && s' == eff_setFlag_2(act, s, para))
 || (pre_setTurn_1(act, s, para) && s' == eff_setTurn_1(act, s, para))
 || (pre_setTurn_2(act, s, para) && s' == eff_setTurn_2(act, s, para))
 || (pre_checkTurn(act, s, para) && s' == eff_checkTurn(act, s, para))
 || (pre_checkFlag(act, s, para) && s' == eff_checkFlag(act, s, para))
 || (pre_critical(act, s, para) && s' == eff_critical(act, s, para))
 || (pre_release_1(act, s, para) && s' == eff_release_1(act, s, para))
 || (pre_release_2(act, s, para) && s' == eff_release_2(act, s, para))
}

lemma Input_Enabled(act: Action, s: State, para: Parameter)
    requires Input(act, para)
    ensures  exists s': State :: Transition(s, act, s', para)
{
    var s': State :| Transition(s, act, s', para);
}

}
