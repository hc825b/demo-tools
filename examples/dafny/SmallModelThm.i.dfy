module SmallModelThm {

predicate is_bounded_sub(f: seq<nat> -> seq<nat>)
{
    forall id_vars: seq<nat> :: |id_vars| == |f(id_vars)|
        && bounded_id(f(id_vars))
}

predicate axiom_substitution(pred: seq<nat> -> bool)
{
    exists f: seq<nat> -> seq<nat> | is_bounded_sub(f) ::
        forall id_vars: seq<nat> :: !pred(id_vars) ==> !pred(f(id_vars))
}

predicate bounded_id(id_vars: seq<nat>)
{
    forall i: nat | i < |id_vars| :: id_vars[i] < |id_vars|
}

lemma lem_composition<T>(Q: T -> bool, f: T -> T)
    requires forall x :: Q(f(x))
    requires forall x :: Q(f(x)) ==> Q(x)
    ensures  forall x :: Q(x)
{
    var P: T -> bool :| forall x :: P(x) <==> Q(f(x));
    assert forall x :: P(x);
    assert forall x :: P(x) ==> Q(x);
}

lemma lem_small_model(pred : seq<nat> -> bool)
    requires axiom_substitution(pred)
    requires forall id_vars: seq<nat> :: bounded_id(id_vars) ==> pred(id_vars)
    ensures  forall id_vars: seq<nat> :: pred(id_vars)
{
    var f: seq<nat> -> seq<nat> :|
           is_bounded_sub(f)
        && forall id_vars: seq<nat> :: !pred(id_vars) ==> !pred(f(id_vars));
    assert forall id_vars: seq<nat> :: bounded_id(f(id_vars));
    assert forall id_vars: seq<nat> :: pred(f(id_vars));
    lem_composition(pred, f);
}

}
