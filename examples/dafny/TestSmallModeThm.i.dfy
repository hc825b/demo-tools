include "SmallModelThm.i.dfy"

module TestSmallModelThm {
import opened SmallModelThm

predicate inv(id_vars: seq<nat>)
{
    |id_vars| >= 2 ==>
    id_vars[0]*id_vars[0] + id_vars[1]*id_vars[1] >= id_vars[0] * id_vars[1]
}

function zero(n: nat): seq<nat>
    ensures |zero(n)| == n
    ensures forall i:nat | i < n :: zero(n)[i] == 0
{
    if n == 0 then [] else zero(n-1) + [0]
}

function f(id_vars: seq<nat>): seq<nat>
    ensures |id_vars| == |f(id_vars)|
    ensures bounded_id(f(id_vars))
{
    zero(|id_vars|)
}

lemma lem_sub(id_vars: seq<nat>)
    requires !inv(id_vars)
    ensures  !inv(f(id_vars))
{
    if |id_vars| >= 2
    {
        assert id_vars[0]*id_vars[0] >= f(id_vars)[0]*f(id_vars)[0];
        assert id_vars[1]*id_vars[1] >= f(id_vars)[1]*f(id_vars)[1];
        assert id_vars[0]*id_vars[1] >= f(id_vars)[0]*f(id_vars)[1];
        assert false;
    }
}

lemma lem_inv()
    ensures forall x0, x1, x2, x3 :: inv([x0, x1, x2, x3])
{
    assert is_bounded_sub(f);
    forall id_vars: seq<nat> | !inv(id_vars)
        ensures !inv(f(id_vars));
    {
        lem_sub(id_vars);
    }
    assert axiom_substitution(inv);
    lem_small_model(inv);
}
}
