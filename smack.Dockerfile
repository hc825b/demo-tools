FROM debian:stretch-slim
# AS build

ENV SMACK_REPO="https://github.com/smackers/smack.git" \
    SMACK_BUILD_DIR=/tmp/smack \
    Z3_DOWNLOAD_LINK="https://github.com/Z3Prover/z3/releases/download/z3-4.6.0/z3-4.6.0-x64-debian-8.10.zip" \
    Z3_DIR=/tmp/z3 \
    BOOGIE_REPO="https://github.com/boogie-org/boogie.git" \
    BOOGIE_COMMIT=c609ee2864 \
    BOOGIE_BUILD_DIR=/tmp/boogie \
    CORRAL_REPO="https://github.com/boogie-org/corral.git" \
    CORRAL_COMMIT=b2738b4839 \
    CORRAL_BUILD_DIR=/tmp/corral \
    LOCKPWN_REPO="https://github.com/smackers/lockpwn.git" \
    LOCKPWN_COMMIT=a4d802a1cb \
    LOCKPWN_BUILD_DIR=/tmp/lockpwn \
    LLVM_VERSION=3.9

RUN apt-get update && apt-get install -y --no-install-recommends \
    clang-${LLVM_VERSION} \
    cmake \
    dirmngr \
    git \
    gnupg \
    libz-dev \
    llvm-${LLVM_VERSION} llvm-${LLVM_VERSION}-dev \
    make \
    python2.7 \
    unzip

# Install mono-devel
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF

RUN echo "deb http://download.mono-project.com/repo/debian stable-stretch main" > /etc/apt/sources.list.d/mono-official-stable.list
RUN apt-get update && apt-get install -y --no-install-recommends \
    mono-devel nuget msbuild

# Build Boogie
RUN git clone ${BOOGIE_REPO} ${BOOGIE_BUILD_DIR}
WORKDIR ${BOOGIE_BUILD_DIR}/Source
RUN git checkout --detach ${BOOGIE_COMMIT}
RUN nuget restore Boogie.sln
RUN msbuild Boogie.sln /p:Configuration=Release

# Build Corral
RUN git clone ${CORRAL_REPO} ${CORRAL_BUILD_DIR}
WORKDIR ${CORRAL_BUILD_DIR}
RUN git checkout --detach ${CORRAL_COMMIT}
RUN git submodule update --init
RUN msbuild cba.sln /p:Configuration=Release

# Build lockpwn
RUN git clone ${LOCKPWN_REPO} ${LOCKPWN_BUILD_DIR}
WORKDIR ${LOCKPWN_BUILD_DIR}
RUN git checkout --detach ${LOCKPWN_COMMIT}
RUN msbuild lockpwn.sln /p:Configuration=Release

# Build Smack
RUN git clone ${SMACK_REPO} ${SMACK_BUILD_DIR}
WORKDIR ${SMACK_BUILD_DIR}
RUN mkdir -p build
WORKDIR build
RUN cmake -DCMAKE_C_COMPILER=clang-${LLVM_VERSION} -DCMAKE_CXX_COMPILER=clang++-${LLVM_VERSION} -DCMAKE_BUILD_TYPE=Debug ../ \
 && make -j4

# Install Z3
ADD ${Z3_DOWNLOAD_LINK} /tmp/z3-downloaded.zip
RUN unzip -o /tmp/z3-downloaded.zip -d /tmp/z3-extracted \
 && mv -f /tmp/z3-extracted/z3-* ${Z3_DIR}

# Install Smack
WORKDIR ${SMACK_BUILD_DIR}/build
RUN make install

# Test Smack
RUN ln -sf ${Z3_DIR}/bin/z3 ${BOOGIE_BUILD_DIR}/Binaries/z3.exe \
 && ln -sf ${Z3_DIR}/bin/z3 ${CORRAL_BUILD_DIR}/bin/Release/z3.exe \
 && ln -sf ${Z3_DIR}/bin/z3 ${LOCKPWN_BUILD_DIR}/Binaries/z3.exe

RUN update-alternatives --install /usr/bin/clang clang /usr/bin/clang-${LLVM_VERSION} 30 \
 && update-alternatives --install /usr/bin/clang++ clang++ /usr/bin/clang++-${LLVM_VERSION} 30 \
 && update-alternatives --install /usr/bin/llvm-config llvm-config /usr/bin/llvm-config-${LLVM_VERSION} 30 \
 && update-alternatives --install /usr/bin/llvm-link llvm-link /usr/bin/llvm-link-${LLVM_VERSION} 30 \
 && update-alternatives --install /usr/bin/llvm-dis llvm-dis /usr/bin/llvm-dis-${LLVM_VERSION} 30

ENV BOOGIE="mono ${BOOGIE_BUILD_DIR}/Binaries/Boogie.exe" \
    CORRAL="mono ${CORRAL_BUILD_DIR}/bin/Release/corral.exe" \
    LOCKPWN="mono ${LOCKPWN_BUILD_DIR}/Binaries/lockpwn.exe" \

WORKDIR ${SMACK_BUILD_DIR}/test
RUN apt-get install -y --no-install-recommends \
    python-yaml python-psutil
RUN python regtest.py --exhaustive --folder=basic
