# -*- coding: utf-8 -*-

from pygments.lexer import RegexLexer, words, bygroups, default
from pygments import token

class TIOALexer(RegexLexer):
    name = 'TIOA'
    aliases = ['tioa']
    filenames = ['*.ioa', '*.tioa']

    keywords_const = ['true', 'false']
    keywords_decl = [
          'automaton'
        , 'components'
        , 'invariant'
        , 'type', 'const'
        , 'discrete'
        , 'signature', 'internal', 'external', 'input', 'output'
        , 'states', 'discrete'
        , 'transitions', 'eff'
        , 'local'
        , 'trajectories', 'evolve'
        ]

    keywords_reserved = [
          'where'
        , 'initially'
        , 'pre'
        , 'if', 'then', 'else'
        , 'case', 'of'
        , 'stop when'
        ]

    keywords_type = ['set', 'seq', 'map', 'nat', u'ℕ', 'int', u'ℤ', 'real', u'ℝ', 'bool', 'string',
                     'Set', 'Seq', 'Map', 'Nat', 'Int', 'Real', 'Bool', 'String',
                     'enumeration', 'tuple']

    operators = [
          ':' # type
        , u'∈' # subset / refinement type
        , u'×' # Cartesian product
        , u'∩', u'∪' # set operation
        , u'⌢' # sequence concatenation
        , '.' # member
        , '+', '-', '*', '/'
        , u'⊕'
        , '=', u'≠', u'≥', u'≤', '>', '<'
        , u'¬', u'∧', u'∨', u'∃', u'∀'
        , ':=' # assign
        , '{|', '|}_' # encryption with the key after underscore
        , u'⊢' # Entail
        , u'‖' # parallel composition
        ]

    operators_extra= [
          '!=', '~=' # Not Equal
        , '>=', '<='
        , '!', '~' # Logic Not
        , '/\\', '&&'
        , '\\/', '||'
        , '\\A' # Forall
        , '\\E' # Exists
        , '|-' # Entail
        ]

    operators_word = ['not', 'and', 'or', 'exists', 'forall', 'xor']

    punctuations = [',', ';', '(', ')', '[', ']', '{', '}', '|']

    tokens = {
        'root': [
            (r'//.*?$', token.Comment.Singleline),
            (r'/\*', token.Comment.Multiline, 'comment'),
            (words(keywords_const), token.Keyword.Constant),
            (words(keywords_decl), token.Keyword.Declaration),
            (words(keywords_reserved), token.Keyword.Reserved),
            (words(keywords_type), token.Keyword.Type),
            (r'bv[1-9]\d*', token.Keyword.Type),
            (words(operators_extra), token.Operator),
            (words(operators), token.Operator),
            (words(operators_word), token.Operator.Word),
            (words(punctuations), token.Punctuation),
            (r'\d+', token.Number),
            (r'"', token.String.Double, 'single-double-quoted-string'),
            (r'[_A-Za-z][_0-9A-Za-z]*', token.Name),
            (r'\s+', token.Whitespace)
        ],
        'comment': [
            (r'[^*/]', token.Comment.Multiline),
            (r'/\*', token.Comment.Multiline, '#push'),
            (r'\*/', token.Comment.Multiline, '#pop'),
            (r'[*/]', token.Comment.Multiline)
        ],
        'single-double-quoted-string': [
            (r'"', token.String.Double, 'end-of-string'),
            (r'[^"\\\n]+', token.String.Double),
            (r'\\', token.String.Double, 'string-escape'),
        ],
        'string-escape': [
            (r'.', token.String.Escape, '#pop'),
        ],
        'end-of-string': [
            (r'(@)([a-z]+(:?-[a-z0-9]+)*)',
            bygroups(token.Operator, token.Name.Function), '#pop:2'),
            (r'\^\^', token.Operator, '#pop:2'),
            default('#pop:2'),
        ],
    }

    def get_tokens_unprocessed(self, text):
        shorthand = { '!=': u'≠', '~=': u'≠'
                    , '<=': u'≤', '>=': u'≥'
                    , '!': u'¬', '~': u'¬'
                    , '/\\': u'∧', '&&': u'∧'
                    , '\\/': u'∨', '||': u'∨'
                    , '\\A': u'∀'
                    , '\\E': u'∃'
                    , '|-': u'⊢'
                    }

        for index, tok, val in RegexLexer.get_tokens_unprocessed(self, text):
            if tok is token.Operator and val in shorthand:
                yield index, tok, shorthand[val]
            else:
                yield index, tok, val


def setup(app):
    app.add_lexer("tioa", TIOALexer())
